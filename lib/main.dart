import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:localization/localization.dart';
import 'package:my_project/generated/locales.g.dart';

import 'app/routes/app_pages.dart';
import 'app/shared/constant.dart';

void main() {
  //WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  //FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    LocalJsonLocalization.delegate.directories = ['lib/i18n'];

    return ScreenUtilInit(
      designSize: const Size(400, 780),
      builder: (context, child) => GetMaterialApp(
        debugShowCheckedModeBanner: false,
        title: APP_TITLE,
        initialRoute: AppPages.INITIAL,
        getPages: AppPages.routes,
        theme: ThemeData().copyWith(
          colorScheme: ThemeData().colorScheme.copyWith(primary: primaryColor),
          errorColor: errorColor,
        ),
        translationsKeys: AppTranslation.translations,
        locale: const Locale('en', 'EN'),
      ),
    );
  }
}
