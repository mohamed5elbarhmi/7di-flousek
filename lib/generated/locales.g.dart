// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

// ignore_for_file: lines_longer_than_80_chars
// ignore: avoid_classes_with_only_static_members
class AppTranslation {
  static Map<String, Map<String, String>> translations = {
    'ar_AR': Locales.ar_AR,
    'en_EN': Locales.en_EN,
  };
}

class LocaleKeys {
  LocaleKeys._();
  static const welcomeTo = 'welcomeTo';
  static const home_title = 'home_title';
  static const app_title = 'app_title';
  static const track_money = 'track_money';
  static const write_credit = 'write_credit';
  static const credit_gave = 'credit_gave';
  static const credit_took = 'credit_took';
  static const creditor_name = 'creditor_name';
  static const credit_amount = 'credit_amount';
  static const credit_motif = 'credit_motif';
  static const credit_date = 'credit_date';
  static const creditor_name_select = 'creditor_name_select';
  static const credit_delete_message = 'credit_delete_message';
  static const credit_delete_confirm = 'credit_delete_confirm';
  static const credit_delete_cancel = 'credit_delete_cancel';
  static const credit_add_button = 'credit_add_button';
  static const tracking_chatr_category = 'tracking_chatr_category';
  static const tracking_chatrt_balance = 'tracking_chatrt_balance';
  static const tracking_save_operation = 'tracking_save_operation';
  static const tracking_other_category = 'tracking_other_category';
  static const tracking_category_month = 'tracking_category_month';
  static const tracking_category_year = 'tracking_category_year';
  static const tracking_from_date = 'tracking_from_date';
  static const tracking_to_date = 'tracking_to_date';
  static const tracking_balance_year = 'tracking_balance_year';
  static const tracking_balance_tabale_date = 'tracking_balance_tabale_date';
  static const tracking_balance_tabale_expense =
      'tracking_balance_tabale_expense';
  static const tracking_chart_no_data_available =
      'tracking_chart_no_data_available';
  static const expense_add_button = 'expense_add_button';
  static const credit_list_page_title = 'credit_list_page_title';
  static const add_credit_page_title = 'add_credit_page_title';
  static const edit_credit_page_title = 'edit_credit_page_title';
  static const detail_credit_page_title = 'detail_credit_page_title';
  static const tracking_home_page_title = 'tracking_home_page_title';
  static const tracking_add_operation_page_title =
      'tracking_add_operation_page_title';
  static const tracking_edit_operation_page_title =
      'tracking_edit_operation_page_title';
  static const tracking_detail_operation_page_title =
      'tracking_detail_operation_page_title';
  static const change_language = 'change_language';
  static const expenses_total = 'expenses_total';
  static const dirham = 'dirham';
  static const Jan = 'Jan';
  static const Feb = 'Feb';
  static const Mar = 'Mar';
  static const Apr = 'Apr';
  static const May = 'May';
  static const Jun = 'Jun';
  static const Jul = 'Jul';
  static const Aug = 'Aug';
  static const Sep = 'Sep';
  static const Oct = 'Oct';
  static const Nov = 'Nov';
  static const Dec = 'Dec';
  static const Home = 'Home';
  static const Bills = 'Bills';
  static const Food1 = 'Food1';
  static const Shopping1 = 'Shopping1';
  static const Clothing = 'Clothing';
  static const Sport = 'Sport';
  static const Education1 = 'Education1';
  static const Other = 'Other';
  static const Housing = 'Housing';
  static const Food = 'Food';
  static const Transportation = 'Transportation';
  static const Healthcare = 'Healthcare';
  static const Debts = 'Debts';
  static const Shopping = 'Shopping';
  static const Education = 'Education';
  static const Entertainment = 'Entertainment';
  static const Business = 'Business';
  static const Savings_Investments = 'Savings_Investments';
  static const Unexpected_Expenses = 'Unexpected_Expenses';
  static const Rent_Bills = 'Rent_Bills';
  static const Dining_Coffee = 'Dining_Coffee';
  static const Medical_Expenses = 'Medical_Expenses';
  static const Loan_Repayments = 'Loan_Repayments';
  static const Books_Courses = 'Books_Courses';
  static const Hobbies_Cinema = 'Hobbies_Cinema';
  static const Dating = 'Dating';
}

class Locales {
  static const ar_AR = {
    'welcomeTo': 'أهلا بك في',
    'home_title': 'أهلا بك في',
    'app_title': 'راقب فلوسك',
    'track_money': 'مراقبة النفقات',
    'write_credit': 'سجل الديون',
    'credit_gave': 'للتحصيل',
    'credit_took': 'للتسديد',
    'creditor_name': 'مول الدين',
    'credit_amount': 'المبلغ',
    'credit_motif': 'السبب',
    'credit_date': 'التاريخ',
    'creditor_name_select': 'الدائن',
    'credit_delete_message': 'هل أنت متأكد أنك تريد حذفه؟',
    'credit_delete_confirm': 'تأكيد',
    'credit_delete_cancel': 'إلغاء',
    'credit_add_button': 'إضافة دين',
    'tracking_chatr_category': 'الفئة',
    'tracking_chatrt_balance': 'النفقات',
    'tracking_save_operation': 'حفظ',
    'tracking_other_category': 'أخرى',
    'tracking_category_month': 'شهر',
    'tracking_category_year': 'سنة',
    'tracking_from_date': 'من تاريخ',
    'tracking_to_date': 'إلى تاريخ',
    'tracking_balance_year': 'اختر سنة',
    'tracking_balance_tabale_date': 'التاريخ',
    'tracking_balance_tabale_expense': 'النفقات',
    'tracking_chart_no_data_available': 'ليس لديك أي عملية محفوظة!',
    'expense_add_button': 'إضافة النفقات',
    'credit_list_page_title': 'سجل الديون',
    'add_credit_page_title': 'إضافة دين',
    'edit_credit_page_title': 'تعديل الدين',
    'detail_credit_page_title': 'تفاصيل الدين',
    'tracking_home_page_title': 'رسم بياني',
    'tracking_add_operation_page_title': 'إضافة النفقات',
    'tracking_edit_operation_page_title': 'تعديل النفقات',
    'tracking_detail_operation_page_title': 'تفاصيل النفقات',
    'change_language': 'تغيير اللغة',
    'expenses_total': 'المجموع',
    'dirham': 'درهم',
    'Jan': 'يناير',
    'Feb': 'فبراير',
    'Mar': 'مارس',
    'Apr': 'أبريل',
    'May': 'مايو',
    'Jun': 'يونيو',
    'Jul': 'يوليو',
    'Aug': 'أغسطس',
    'Sep': 'سبتمبر',
    'Oct': 'أكتوبر',
    'Nov': 'نوفمبر',
    'Dec': 'ديسمبر',
    'Home': 'البيت',
    'Bills': 'الفواتير',
    'Food1': 'الطعام',
    'Shopping1': 'التسوق',
    'Clothing': 'الملابس',
    'Sport': 'الرياضة',
    'Education1': 'التعليم',
    'Other': 'أخرى',
    'Housing': 'سكن',
    'Food': 'طعام',
    'Transportation': 'نقل',
    'Healthcare': 'رعاية الصحة',
    'Debts': 'ديون',
    'Shopping': 'تسوق',
    'Education': 'تعليم',
    'Entertainment': 'ترفيه',
    'Business': 'أعمال',
    'Savings_Investments': 'توفير واستثمارات',
    'Unexpected_Expenses': 'نفقات غير متوقعة',
    'Rent_Bills': 'الإيجار، الفواتير...',
    'Dining_Coffee': 'تناول الطعام، القهوة...',
    'Medical_Expenses': 'نفقات طبية...',
    'Loan_Repayments': 'سداد القروض...',
    'Books_Courses': 'كتب، دورات...',
    'Hobbies_Cinema': 'هوايات، السينما...',
    'Dating': 'المواعيد، اللقاءات...',
  };
  static const en_EN = {
    'welcomeTo': 'Welcome To ',
    'home_title': 'Welcome To ',
    'app_title': 'Ra9eb Flousek',
    'track_money': 'Expense monitoring',
    'write_credit': 'Credit monitoring',
    'credit_gave': 'To Collect',
    'credit_took': 'To Give',
    'creditor_name': 'Moul Credit',
    'credit_amount': 'Amount',
    'credit_motif': 'Motif',
    'credit_date': 'Date',
    'creditor_name_select': 'Creditor',
    'credit_delete_message': 'Are you sure you want to delete it !',
    'credit_delete_confirm': 'Confirm',
    'credit_delete_cancel': 'Cancel',
    'credit_add_button': 'Add credit',
    'tracking_chatr_category': 'Category',
    'tracking_chatrt_balance': 'Expenses',
    'tracking_save_operation': 'Save',
    'tracking_other_category': 'Other',
    'tracking_category_month': 'Month',
    'tracking_category_year': 'Year',
    'tracking_from_date': 'From Date',
    'tracking_to_date': 'To Date',
    'tracking_balance_year': 'Select year',
    'tracking_balance_tabale_date': 'Date',
    'tracking_balance_tabale_expense': 'Expenses',
    'tracking_chart_no_data_available': 'You have no operation saved !',
    'expense_add_button': 'Add expense',
    'credit_list_page_title': 'Credit book',
    'add_credit_page_title': 'Add credit',
    'edit_credit_page_title': 'Edit credit',
    'detail_credit_page_title': 'Credit details',
    'tracking_home_page_title': 'Chart',
    'tracking_add_operation_page_title': 'Add expense',
    'tracking_edit_operation_page_title': 'Edit expense',
    'tracking_detail_operation_page_title': 'Expense details',
    'change_language': 'Change language',
    'expenses_total': 'Total',
    'dirham': 'Dh',
    'Jan': 'Jan',
    'Feb': 'Feb',
    'Mar': 'Mar',
    'Apr': 'Apr',
    'May': 'May',
    'Jun': 'Jun',
    'Jul': 'Jul',
    'Aug': 'Aug',
    'Sep': 'Sep',
    'Oct': 'Oct',
    'Nov': 'Nov',
    'Dec': 'Dec',
    'Home': 'Home',
    'Bills': 'Bills',
    'Food1': 'Food',
    'Shopping1': 'Shopping',
    'Clothing': 'Clothing',
    'Sport': 'Sport',
    'Education1': 'Education',
    'Housing': 'Housing',
    'Food': 'Food',
    'Transportation': 'Transportation',
    'Healthcare': 'Healthcare',
    'Debts': 'Debts',
    'Shopping': 'Shopping',
    'Education': 'Education',
    'Entertainment': 'Entertainment',
    'Business': 'Business',
    'Savings_Investments': 'Savings and Investments',
    'Unexpected_Expenses': 'Unexpected Expenses',
    'Other': 'Other',
    'Rent_Bills': 'Rent, Bills...',
    'Dining_Coffee': 'Dining, Coffee ...',
    'Medical_Expenses': 'Medical Expenses...',
    'Loan_Repayments': 'Loan Repayments...',
    'Books_Courses': 'Books , Courses...',
    'Hobbies_Cinema': 'Hobbies , Cinema...',
  };
}
