import 'package:my_project/app/data/trackingCategoryModel.dart';
import 'package:sqflite/sqflite.dart';

class CategoryDbProvider {
  Future<Database> init() async {
    return openDatabase(
        //open the database or create a database if there isn't any
        '7di_Flousek_category.db',
        version: 1, onCreate: (Database db, int version) async {
      await db.execute("""CREATE TABLE IF NOT EXISTS category(
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            name VARCHAR(50) NOT NULL,
            description VARCHAR(80))""");
    });
  }

  Future<List<TrackingCategoryModel>> getCategoryList() async {
    final db = await init();
    final maps = await db.query('category', orderBy: "id");
    return List.generate(maps.length, (i) {
      return TrackingCategoryModel.fromJson(maps[i]);
    });
  }

  Future<TrackingCategoryModel> getCategoryById(id) async {
    final db = await init();
    var maps =
        await db.query('category', where: "id = ?", whereArgs: [id], limit: 1);
    return TrackingCategoryModel.fromJson(maps[0]);
  }

  Future<int> addCategory(TrackingCategoryModel cat) async {
    final db = await init();
    return db.insert(
      "category",
      cat.toJson(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> updateCategoryID(TrackingCategoryModel cat) async {
    final db = await init();
    cat.id = null;
    int result = await db
        .update("category", cat.toJson(), where: "id = ?", whereArgs: [cat.id]);
    return result;
  }

  Future<int> deleteCategoryById(int id) async {
    final db = await init();

    int result = await db.delete("category", where: "id = ?", whereArgs: [id]);

    return result;
  }

  Future<int> deleteAllCategories() async {
    final db = await init();

    int result = await db.delete("category");

    return result;
  }
}
