import 'dart:developer';
import 'dart:io';
import 'package:my_project/app/data/creditModel.dart';
import 'package:sqflite/sqflite.dart';

class CreditDbProvider {
  Future<Database> init() async {
    return openDatabase(
        //open the database or create a database if there isn't any
        '7di_Flousek_credit.db',
        version: 1, onCreate: (Database db, int version) async {
      await db.execute("""CREATE TABLE IF NOT EXISTS credit(
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            creditor VARCHAR(50) NOT NULL,
            createdAt DATETIME NOT NULL ,
            isCredit BOOLEAN NOT NULL ,
            motif TEXT NOT NULL,
            amount DOUBLE NOT NULL)""");
    });
  }

  Future<List<CreditModel>> getCreditList(isCredit) async {
    final db = await init();
    final maps = await db.query('credit',
        where: "isCredit = ?", whereArgs: [isCredit], orderBy: "id");
    return List.generate(maps.length, (i) {
      return CreditModel.fromJson(maps[i]);
    });
  }

  Future<List<CreditModel>> getCreditListByNameAndIsCredit(
      creditor, isCredit) async {
    final db = await init();
    final maps = await db.query('credit',
        where: "creditor = ? and isCredit = ?",
        whereArgs: [creditor, isCredit],
        orderBy: "id");
    return List.generate(maps.length, (i) {
      return CreditModel.fromJson(maps[i]);
    });
  }

  Future<List<CreditModel>> getCreditorNameList() async {
    final db = await init();
    final maps = await db.query('credit',
        //where: "isCredit = ?",
        //whereArgs: [isCredit],
        orderBy: "id",
        groupBy: "creditor");
    return List.generate(maps.length, (i) {
      return CreditModel.fromJson(maps[i]);
    });
  }

  Future<CreditModel> getCreditById(id) async {
    final db = await init();
    var maps =
        await db.query('credit', where: "id = ?", whereArgs: [id], limit: 1);
    return CreditModel.fromJson(maps[0]);
  }

  Future<int> addCredit(CreditModel credit) async {
    final db = await init();

    return db.insert(
      "credit",
      credit.toJson(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<int> updateCreditByID(CreditModel credit) async {
    final db = await init();

    int result = await db.update("credit", credit.toJson(),
        where: "id = ?", whereArgs: [credit.id]);
    return result;
  }

  Future<int> deleteCreditById(int id) async {
    final db = await init();

    int result = await db.delete("credit", where: "id = ?", whereArgs: [id]);

    return result;
  }
}
