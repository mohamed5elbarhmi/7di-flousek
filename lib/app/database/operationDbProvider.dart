import 'dart:developer';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:my_project/app/data/creditModel.dart';
import 'package:my_project/app/data/operationModel.dart';
import 'package:sqflite/sqflite.dart';

class OperationDbProvider {
  Future<Database> init() async {
    return openDatabase(
        //open the database or create a database if there isn't any
        '7di_Flousek.db',
        version: 1, onCreate: (Database db, int version) async {
      await db.execute("""CREATE TABLE IF NOT EXISTS operation(
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            createdAt DATETIME NOT NULL ,
            motif TEXT NOT NULL,
            amount DOUBLE NOT NULL,
            category VARCHAR(50) NOT NULL)""");
    });
  }

  Future<List<OperationModel>> getOperationList() async {
    final db = await init();
    final maps = await db.query('operation', orderBy: "createdAt");
    return List.generate(maps.length, (i) {
      return OperationModel.fromJson(maps[i]);
    });
  }

  Future<List<OperationModel>> getOperationListByDateInterval(
      DateTime from, DateTime to) async {
    final db = await init();
    final maps = await db.query('operation',
        where: "createdAt >= ? AND createdAt <= ?",
        whereArgs: [from.toString(), to.toString()],
        orderBy: "createdAt");
    return List.generate(maps.length, (i) {
      return OperationModel.fromJson(maps[i]);
    });
  }

  Future<List<OperationModel>> getOperationListByDateIntervalAndCategory(
      DateTime from, DateTime to, String cat) async {
    final db = await init();
    final maps = await db.query('operation',
        where: "createdAt >= ? AND createdAt <= ? AND category = ?",
        whereArgs: [from.toString(), to.toString(), cat],
        orderBy: "createdAt DESC");
    return List.generate(maps.length, (i) {
      return OperationModel.fromJson(maps[i]);
    });
  }

  Future<List<Map<String, dynamic>>> getCategoryAndSumByDateInterval(
      DateTime from, DateTime to) async {
    final db = await init();
    final result = await db.rawQuery('''
      SELECT category, SUM(amount) AS totalAmount
      FROM operation
      WHERE createdAt >= ? AND createdAt <= ?
      GROUP BY category
      ORDER BY totalAmount DESC
    ''', [from.toString(), to.toString()]);

    return result;
  }

  Future<List<Map<String, dynamic>>> getOperationsByYearAndMonth(
      int year) async {
    final db = await init();
    return db.rawQuery('''
    SELECT 
      strftime('%Y', createdAt) AS year,
      strftime('%m', createdAt) AS month,
      SUM(amount) AS totalAmount
    FROM operation
    WHERE strftime('%Y', createdAt) = ?
    GROUP BY year, month
  ''', ['$year']);
  }

  Future<double?> getTotalAmountBetweenDates(DateTime from, DateTime to) async {
    final db = await init();

    final result = await db.rawQuery('''
      SELECT SUM(amount) AS totalAmount
      FROM operation
      WHERE createdAt >= ? AND createdAt <= ?
    ''', [from.toString(), to.toString()]);

    try {
      var totalResult = double.parse(result.first['totalAmount'].toString());
      return totalResult;
    } catch (e) {
      return 0.0;
    }
  }

  Future<OperationModel> getOperationById(id) async {
    final db = await init();
    var maps =
        await db.query('operation', where: "id = ?", whereArgs: [id], limit: 1);
    return OperationModel.fromJson(maps[0]);
  }

  Future<int> addOperation(OperationModel op) async {
    final db = await init();

    return db.insert(
      "operation",
      op.toJson(),
      conflictAlgorithm: ConflictAlgorithm.ignore,
    );
  }

  Future<List<Map<String, Object?>>> getYearList() async {
    final db = await init();
    final maps = await db.rawQuery('''
      SELECT strftime('%Y', createdAt) AS year FROM operation GROUP BY year ORDER BY year
    ''');
    return maps;
  }

  Future<int> updateOperationByID(OperationModel op) async {
    final db = await init();

    int result = await db
        .update("operation", op.toJson(), where: "id = ?", whereArgs: [op.id]);
    return result;
  }

  Future<int> deleteOperationById(int id) async {
    final db = await init();

    int result = await db.delete("operation", where: "id = ?", whereArgs: [id]);

    return result;
  }
}
