class CreditModel {
  int? id;
  String? creditor;
  DateTime? createdAt;
  bool? isCredit;
  String? motif;
  double? amount;

  CreditModel(
      {this.id,
      this.creditor,
      this.createdAt,
      this.isCredit,
      this.motif,
      this.amount});

  CreditModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    creditor = json['creditor'];
    createdAt = DateTime.parse(json['createdAt'].toString());
    isCredit = json['isCredit'] == 1 ? true : false;
    motif = json['motif'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return <String, dynamic>{
      "id": id,
      "creditor": creditor,
      "createdAt": createdAt.toString(),
      "isCredit": isCredit,
      "motif": motif,
      "amount": amount
    };
  }
}
