class ChartData {
  int? x;
  double? y;

  ChartData({this.x, this.y});
}
