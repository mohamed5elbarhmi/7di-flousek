import 'package:my_project/app/data/trackingCategoryModel.dart';

class OperationModel {
  int? id;
  String? category;
  DateTime? createdAt;
  String? motif;
  double? amount;

  OperationModel(
      {this.id, this.category, this.createdAt, this.motif, this.amount});

  OperationModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    category = json['category'];
    createdAt = DateTime.parse(json['createdAt'].toString());
    motif = json['motif'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return <String, dynamic>{
      "id": id,
      "category": category,
      "createdAt": createdAt.toString(),
      "motif": motif,
      "amount": amount
    };
  }
}
