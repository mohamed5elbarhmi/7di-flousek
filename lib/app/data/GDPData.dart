class GDPData {
  double? gdp;
  String? content;

  GDPData({this.gdp, this.content});
}
