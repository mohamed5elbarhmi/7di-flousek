class TrackingCategoryModel {
  int? id;
  String? name;
  String? description;

  TrackingCategoryModel({this.id, this.name, this.description});

  TrackingCategoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      "id": id,
      "name": name,
      "description": description
    };
  }
}
