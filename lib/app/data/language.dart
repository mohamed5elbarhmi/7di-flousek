import 'package:flutter/material.dart';

class Language {
  String? name;
  Locale? locale;

  Language({this.name, this.locale});
}
