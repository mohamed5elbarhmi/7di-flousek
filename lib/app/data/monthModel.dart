class monthModel {
  int? id;
  String? name;

  monthModel({this.id, this.name});

  monthModel.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id']);
    name = json['name'];
  }
}
