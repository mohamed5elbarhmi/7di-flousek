class CreditGroupModel {
  String? creditor;
  double? amount;

  CreditGroupModel({this.creditor, this.amount});
}
