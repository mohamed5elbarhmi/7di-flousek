import 'dart:developer';

import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:my_project/app/routes/app_pages.dart';
import 'package:my_project/app/shared/constant.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_constructors
    return GetBuilder<HomeController>(
      init: HomeController(), //controller,
      builder: (controller) => Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(top: 60.h),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20.h,
                    ),
                    SizedBox(
                      height: 65.h,
                      child: Image.asset(homeLogoPath),
                    ),
                    SizedBox(
                      height: 20.h,
                    ),
                    SizedBox(
                        width: 300.w,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("home_title".tr,
                                style: TextStyle(
                                    fontSize: 22.sp, color: Colors.black)),
                            SizedBox(
                              width: Get.locale!.toLanguageTag() == "ar-AR"
                                  ? 10.w
                                  : 4.w,
                            ),
                            Text(APP_TITLE,
                                style: TextStyle(
                                    fontSize: 22.sp,
                                    fontWeight: FontWeight.bold,
                                    color: primaryColor))
                          ],
                        )),
                    SizedBox(
                      height: 30.h,
                    ),
                    home_Item_nav(trackMoneyImgPath, "track_money".tr,
                        Routes.TRACKING_HOME),
                    home_Item_nav(
                        creditImgPath, "write_credit".tr, Routes.CREDIT_LIST),
                    SizedBox(
                      height: 20.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: 80.w,
                          height: 1.w,
                          color: Colors.grey,
                        ),
                        Text(
                          "change_language".tr,
                          style: TextStyle(
                              fontSize: 18.sp, fontWeight: FontWeight.w400),
                        ),
                        Container(
                          width: 80.w,
                          height: 1.w,
                          color: Colors.grey,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 30.h,
                    ),
                    Directionality(
                      textDirection: TextDirection.rtl,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          for (var ln in getLanguageList())
                            GestureDetector(
                              onTap: () {
                                Get.updateLocale(ln.locale!);
                              },
                              child: Container(
                                width: 100.w,
                                height: 40.h,
                                decoration: BoxDecoration(
                                    color: Get.locale!.toLanguageTag() ==
                                            ln.locale!.toLanguageTag()
                                        ? primaryColor
                                        : inputBackColor,
                                    borderRadius: BorderRadius.circular(10.r)),
                                child: Center(
                                    child: Text(
                                  ln.name!,
                                  style: TextStyle(
                                    fontSize: 18.sp,
                                    color: Get.locale!.toLanguageTag() ==
                                            ln.locale!.toLanguageTag()
                                        ? Colors.white
                                        : Colors.black,
                                  ),
                                )),
                              ),
                            )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          )),
    );
  }

//"assets/images/icon_trouver_une_consigne.png"
// ignore: non_constant_identifier_names
  Widget home_Item_nav(imageName, title, pageName) {
    return GestureDetector(
      onTap: () {
        Get.toNamed(pageName);
      },
      child: Padding(
        padding:
            EdgeInsets.only(top: 16.h, bottom: 16.h, right: 30.w, left: 30.w),
        child: Container(
          height: 90.w,
          decoration: BoxDecoration(
              color: inputBackColor, borderRadius: BorderRadius.circular(10.r)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 55.h,
                child: Image.asset(imageName),
              ),
              SizedBox(
                child: Text(
                  title,
                  style:
                      TextStyle(fontSize: 18.sp, fontWeight: FontWeight.w500),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
