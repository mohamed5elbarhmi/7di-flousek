import 'package:get/get.dart';
import 'package:my_project/app/modules/credit/details_credit/controllers/details_credit_controller.dart';

class DetailsCreditBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => DetailsCreditController(),
    );
  }
}
