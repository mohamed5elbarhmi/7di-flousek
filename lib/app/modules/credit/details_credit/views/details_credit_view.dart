import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:my_project/app/data/creditModel.dart';
import 'package:my_project/app/routes/app_pages.dart';
import 'package:my_project/app/shared/appBar.dart';
import 'package:my_project/app/shared/constant.dart';

import '../controllers/details_credit_controller.dart';

class DetailsCreditView extends GetView<DetailsCreditController> {
  const DetailsCreditView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DetailsCreditController>(
      init: DetailsCreditController(), //controller,
      builder: (c) => Directionality(
        textDirection: TextDirection.ltr,
        child: Scaffold(
            appBar: getStandardAppBar(DETAIL_CREDIT_PAGE_TITLE),
            floatingActionButton: !c.isDataProcessing.value
                ? FloatingActionButton.extended(
                    onPressed: () {
                      Get.toNamed(Routes.ADD_CREDIT, arguments: [
                        c.isCredit.value,
                        c.CreditorName.value,
                        null,
                        false // from detail page = false, from list page =  true
                      ]);
                    },
                    label: Text(
                      "credit_add_button".tr,
                      style: TextStyle(fontSize: 18.sp),
                    ),
                    icon: Icon(Icons.add, size: 36.sp),
                    backgroundColor: primaryColor,
                  )
                : null,
            body: Center(
                child: c.isDataProcessing.value
                    ? LoadingAnimationWidget.newtonCradle(
                        color: primaryColor, size: 240.w)
                    : Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                            Container(
                              width: 360.w,
                              height: 50.h,
                              color: Colors.white,
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 20.w,
                                  ),
                                  Container(
                                    width: 40.w,
                                    height: 40.w,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(100.r)),
                                        border:
                                            Border.all(color: primaryColor)),
                                    child: Icon(
                                      Icons.person,
                                      color: primaryColor,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 60.w,
                                  ),
                                  Text(
                                    "${c.CreditorName.toUpperCase()}  :  ${c.total.toString()} ${"dirham".tr}",
                                    style: TextStyle(
                                        fontSize: 20.sp,
                                        fontWeight: FontWeight.w500,
                                        color: primaryColor),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              color: const Color.fromARGB(255, 199, 199, 199),
                              height: 6.h,
                            ),
                            Expanded(
                              child: ListView.builder(
                                itemCount: c.creditList.length,
                                itemBuilder: (BuildContext context, int index) {
                                  final CreditModel item = c.creditList[index];
                                  return Column(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          // show crud actions
                                          c.selectItem(item.id);
                                        },
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              top: 20.h,
                                              left: 30.w,
                                              right: 30.w),
                                          child: Container(
                                            width: 320.w,
                                            height: 42.h,
                                            decoration: BoxDecoration(
                                              color: inputBackColor,
                                              borderRadius:
                                                  BorderRadius.circular(8.r),
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                Text(
                                                  "${c.prepareCreatedAt(item.createdAt!)}   :",
                                                  style: TextStyle(
                                                      fontSize: 18.sp,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                                Text(
                                                  "${item.amount} ${"dirham".tr}",
                                                  style: TextStyle(
                                                      fontSize: 18.sp,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                                c.idElementDisplayed.value ==
                                                        item.id
                                                    ? Icon(
                                                        Icons.keyboard_arrow_up,
                                                        size: 38.w,
                                                      )
                                                    : Icon(
                                                        Icons
                                                            .keyboard_arrow_down,
                                                        size: 38.w,
                                                      )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      if (c.idElementDisplayed.value == item.id)
                                        Container(
                                          width: 320.w,
                                          height: 90.h,
                                          decoration: BoxDecoration(
                                            color: detailBlockColor,
                                            borderRadius: BorderRadius.only(
                                                bottomLeft:
                                                    Radius.circular(10.r),
                                                bottomRight:
                                                    Radius.circular(10.r)),
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                height: 5.h,
                                              ),
                                              SizedBox(
                                                height: 35.h,
                                                child: Directionality(
                                                  textDirection: Get.locale!
                                                              .toLanguageTag() ==
                                                          "ar-AR"
                                                      ? TextDirection.rtl
                                                      : TextDirection.ltr,
                                                  child: Text(
                                                    "${"credit_motif".tr} : ${item.motif!}",
                                                    style: TextStyle(
                                                        fontSize: 18.sp,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height: 50.h,
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceEvenly,
                                                  children: [
                                                    IconButton(
                                                      icon: Icon(
                                                        Icons.edit_document,
                                                        color: Colors.blue,
                                                        size: 34.w,
                                                      ),
                                                      onPressed: () {
                                                        Get.toNamed(
                                                            Routes.ADD_CREDIT,
                                                            arguments: [
                                                              c.isCredit.value,
                                                              c.CreditorName
                                                                  .value,
                                                              item.id,
                                                              false
                                                            ]);
                                                      },
                                                    ),
                                                    IconButton(
                                                      icon: Icon(
                                                        Icons.delete,
                                                        color: Colors.red,
                                                        size: 34.w,
                                                      ),
                                                      onPressed: () {
                                                        //c.deleteCredit(item.id);
                                                        c.showDeleteModal(
                                                            item.id);
                                                      },
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        )
                                    ],
                                  );
                                },
                              ),
                            ),
                          ]))),
      ),
    );
  }
}
