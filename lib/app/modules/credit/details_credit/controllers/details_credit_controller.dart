import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:my_project/app/database/creditDbProvider.dart';
import 'package:my_project/app/shared/constant.dart';

import '../../credit_list/controllers/credit_list_controller.dart';

class DetailsCreditController extends GetxController {
  //TODO: Implement DetailsCreditController

  CreditDbProvider creditDb = CreditDbProvider();
  var CreditorName = ''.obs;
  var isCredit = true.obs;
  var creditList = [].obs;
  var total = 0.0.obs;
  var isDataProcessing = false.obs;

  var idElementDisplayed = 100000.obs;

  @override
  void onInit() {
    CreditorName.value = Get.arguments[0];
    isCredit.value = Get.arguments[1];
    getCreditListByCreditorName();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    Get.find<CreditListController>().onInit();
  }

  void getCreditListByCreditorName() async {
    isDataProcessing.value = true;
    creditList.value = await creditDb.getCreditListByNameAndIsCredit(
        CreditorName.value, isCredit.value);
    total.value = 0.0;
    for (var element in creditList) {
      total.value += element.amount!;
    }
    isDataProcessing.value = false;
    update();
  }

  String prepareCreatedAt(DateTime value) {
    return DateFormat("dd/MM/y").format(value).toString();
  }

  void selectItem(value) {
    if (idElementDisplayed.value == value) {
      idElementDisplayed.value = 1000000;
    } else {
      idElementDisplayed.value = value;
    }
    update();
  }

  Future showDeleteModal(creditId) {
    return Get.defaultDialog(
      title: "",
      content: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.warning,
            color: Colors.red,
            size: 40,
          ),
          SizedBox(
            height: 14.h,
          ),
          Text(
            "credit_delete_message".tr,
            style: TextStyle(fontSize: 16),
          ),
          SizedBox(
            height: 16.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                child: Text("credit_delete_cancel".tr),
                style: ElevatedButton.styleFrom(
                  primary: Colors.grey,
                  elevation: 0,
                ),
                onPressed: () {
                  Get.back();
                },
              ),
              ElevatedButton(
                child: Text("credit_delete_confirm".tr),
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  elevation: 0,
                ),
                onPressed: () {
                  deleteCredit(creditId);
                  Get.back();
                },
              ),
            ],
          )
        ],
      ),
    );
  }

  void deleteCredit(creditId) async {
    await creditDb.deleteCreditById(creditId);
    update();
    getCreditListByCreditorName();
  }
}
