import 'package:get/get.dart';
import 'package:my_project/app/shared/validators.dart';

import '../controllers/add_credit_controller.dart';

class AddCreditBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AddCreditController());
    Get.lazyPut(() => ValidateInput());
  }
}
