import 'dart:developer';

import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:my_project/app/shared/appBar.dart';
import 'package:my_project/app/shared/validators.dart';

import '../../../../shared/constant.dart';
import '../../../../shared/input_filed.dart';
import '../controllers/add_credit_controller.dart';

class AddCreditView extends GetView<AddCreditController> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AddCreditController>(
        init: AddCreditController(), //controller,
        builder: (controller) => Scaffold(
            appBar: getStandardAppBar(controller.edit.value
                ? "edit_credit_page_title".tr
                : "add_credit_page_title".tr),
            body: SingleChildScrollView(
              child: Directionality(
                textDirection: TextDirection.ltr,
                child: Center(
                  child: (Form(
                    key: controller.credit_formKey,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 16.h,
                          ),
                          controller.nameList.length > 0
                              ? Container(
                                  padding: EdgeInsets.only(left: 20.w),
                                  width: 340.w,
                                  height: 44.h,
                                  decoration: BoxDecoration(
                                      color: inputBackColor,
                                      borderRadius: BorderRadius.circular(8.r),
                                      border: Border.all(
                                          color: primaryColor, width: 2.sp)),
                                  child: DropDownTextField(
                                    controller: controller.creditorNameDd,
                                    textFieldDecoration: InputDecoration(
                                        contentPadding:
                                            EdgeInsets.only(top: 0.sp),
                                        focusedBorder: UnderlineInputBorder(
                                          //<-- SEE HERE
                                          borderSide: BorderSide(
                                              width: 0.sp, color: Colors.white),
                                        ),
                                        hintText: "creditor_name_select".tr),
                                    dropDownList:
                                        controller.createDropDownList(),
                                    dropDownItemCount: 5,
                                    // controller: controller.creditorNameController,
                                    onChanged: (value) {
                                      controller.creditorNameController.text =
                                          (value as DropDownValueModel).value;
                                    },
                                  ),
                                )
                              : const Text(''),
                          SizedBox(
                            height: 30.h,
                          ),
                          getTextField(
                              // ignore: prefer_const_constructors
                              prefixIcon: Icon(
                                Icons.person,
                                color: primaryColor,
                              ),
                              controllerAttribut:
                                  controller.creditorNameController,
                              hint: "creditor_name_select".tr,
                              myValidator: ValidateInput.validator),
                          SizedBox(
                            height: 30.h,
                          ),
                          getTextField(
                              keyboardType: TextInputType.phone,
                              // ignore: prefer_const_constructors
                              prefixIcon: Icon(
                                Icons.money_outlined,
                                color: primaryColor,
                              ),
                              controllerAttribut: controller.amountController,
                              hint: "credit_amount".tr,
                              myValidator: ValidateInput.validatorAmount),
                          SizedBox(
                            height: 30.h,
                          ),
                          getTextField(
                            keyboardType: TextInputType.datetime,
                            readOnly: true,
                            // ignore: prefer_const_constructors
                            prefixIcon: Icon(
                              Icons.date_range,
                              color: primaryColor,
                            ),
                            controllerAttribut: controller.dateController,
                            hint: "credit_date".tr,
                            onTap: () {
                              controller.chooseDate();
                            },
                            myValidator: ValidateInput.validator,
                          ),
                          SizedBox(
                            height: 30.h,
                          ),
                          getTextField(
                              maxline: 4,
                              // ignore: prefer_const_constructors
                              prefixIcon: Icon(
                                Icons.textsms,
                                color: primaryColor,
                              ),
                              controllerAttribut: controller.motifController,
                              hint: "credit_motif".tr,
                              myValidator: ValidateInput.validator),
                          SizedBox(
                            height: 40.h,
                          ),
                          Container(
                            width: 360.w,
                            height: 46.h,
                            decoration: BoxDecoration(
                                //color: secondaryColor,
                                borderRadius: BorderRadius.circular(6.r)),
                            child: ElevatedButton(
                              onPressed: controller.edit.value
                                  ? controller.updateCredit
                                  : controller.createCredit,
                              style: ElevatedButton.styleFrom(
                                backgroundColor: primaryColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.r),
                                ),
                              ),
                              child: Text(
                                "tracking_save_operation".tr,
                                style: TextStyle(
                                    fontSize: 20.sp, color: Colors.white),
                              ),
                            ),
                          )
                        ]),
                  )),
                ),
              ),
            )));
  }
}
