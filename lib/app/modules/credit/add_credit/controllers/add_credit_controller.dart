import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:my_project/app/data/creditModel.dart';
import 'package:my_project/app/modules/credit/credit_list/controllers/credit_list_controller.dart';
import 'package:my_project/app/modules/credit/details_credit/controllers/details_credit_controller.dart';
import 'package:my_project/app/shared/validators.dart';

import '../../../../database/creditDbProvider.dart';

class AddCreditController extends GetxController {
  CreditDbProvider creditDb = CreditDbProvider();
  ValidateInput validators = ValidateInput();
  // ignore: non_constant_identifier_names
  final credit_formKey = GlobalKey<FormState>();

  var creditorNameController = TextEditingController();
  var dateController = TextEditingController();
  var motifController = TextEditingController();
  var amountController = TextEditingController();

  var creditorNameDd = SingleValueDropDownController();

  var selectedDate = DateTime.now().obs;
  var nameList = <String>[].obs;
  var selectedValue = "";
  var isCredit = true.obs;
  late var creditId;
  var edit = false.obs;
  var fromListPage = true.obs;

  @override
  void onInit() {
    super.onInit();
    isCredit.value = Get.arguments[0];
    creditorNameController.text = Get.arguments[1] ?? '';
    creditorNameDd.dropDownValue = DropDownValueModel(
        name: creditorNameController.text, value: creditorNameController.text);
    checkIfEditOrCreate(Get.arguments[2]);
    fromListPage.value = Get.arguments[3];
    getNameList();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void createCredit() async {
    if (credit_formKey.currentState!.validate()) {
      CreditModel credit = CreditModel();
      credit.creditor = creditorNameController.text;
      credit.amount = double.parse(amountController.text);
      credit.createdAt = selectedDate.value;
      credit.motif = motifController.text;
      credit.isCredit = isCredit.value;

      //create credit
      await creditDb.addCredit(credit).then((value) => {
            if (value != null)
              {
                if (fromListPage.value)
                  {Get.find<CreditListController>().onInit(), Get.back()}
                else
                  {
                    Get.find<DetailsCreditController>()
                        .getCreditListByCreditorName(),
                    Get.back()
                  }
              }
          });
    }
  }

  void preparePageToEditMode() async {
    CreditModel credit = await creditDb.getCreditById(creditId);
    creditorNameController.text = credit.creditor!;
    amountController.text = credit.amount.toString();
    selectedDate.value = credit.createdAt!;
    dateController.text =
        DateFormat("dd/MM/y").format(selectedDate.value).toString();
    motifController.text = credit.motif!;
    update();
  }

  void checkIfEditOrCreate(value) {
    if (value != null) {
      creditId = value;
      edit.value = true;
      preparePageToEditMode();
    } else {
      creditId = 1000000;
    }
  }

  void updateCredit() async {
    if (credit_formKey.currentState!.validate()) {
      CreditModel credit = CreditModel();
      credit.id = creditId;
      credit.creditor = creditorNameController.text;
      credit.amount = double.parse(amountController.text);
      credit.createdAt = selectedDate.value;
      credit.motif = motifController.text;
      credit.isCredit = isCredit.value;

      //create credit
      await creditDb.updateCreditByID(credit).then((value) => {
            if (value != null)
              {
                Get.find<DetailsCreditController>()
                    .getCreditListByCreditorName(),
                Get.back()
              }
          });
    }
  }

/*
  String? validator(String? value) {
    return validator(value);
  }

  String? validatorAmountr(String? value) {
    double result = double.parse(value!);
    if (result.isEqual(0.0) || result <= 0) {
      return "Please enter a numeric value";
    }
    return null;
  }
  */

  void getNameList() async {
    var maps = await creditDb.getCreditorNameList();
    maps.toList().forEach((element) {
      nameList.add(element.creditor!);
    });
    update();
  }

  List<DropDownValueModel> createDropDownList() {
    List<DropDownValueModel> list = [];
    nameList.forEach((element) {
      list.add(DropDownValueModel(name: element, value: element));
    });
    return list;
  }

  chooseDate() async {
    DateTime? pickedDate = await showDatePicker(
      context: Get.context!,
      initialDate: selectedDate.value,
      firstDate: DateTime(2000),
      lastDate: DateTime(2024),
      helpText: 'Select DOB',
      cancelText: 'Close',
      confirmText: 'Confirm',
      errorFormatText: 'Enter valid date',
      errorInvalidText: 'Enter valid date range',
      fieldLabelText: 'DOB',
      fieldHintText: 'Month/Date/Year',
    );
    if (pickedDate != null && pickedDate != selectedDate.value) {
      selectedDate.value = pickedDate;
      dateController.text =
          DateFormat("dd/MM/y").format(selectedDate.value).toString();
      update();
    }
  }
}
