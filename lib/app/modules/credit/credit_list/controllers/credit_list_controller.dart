import 'dart:developer';

import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:my_project/app/data/creditGroupModel.dart';
import 'package:my_project/app/data/creditModel.dart';
import 'package:my_project/app/database/creditDbProvider.dart';

class CreditListController extends GetxController {
  //TODO: Implement CreditListController

  PageController pageController = PageController();
  CreditDbProvider creditDb = CreditDbProvider();
  List<CreditModel> creditList = [];
  var groupedItems = <String, double>{}.obs;
  RxDouble total = 0.0.obs;
  RxBool isDataProcessing = false.obs;
  RxBool isCredit = true.obs;
  @override
  void onInit() {
    getCreditListFromDb();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void getCreditListFromDb() async {
    isDataProcessing.value = true;
    creditList = [];
    total.value = 0.0;
    await creditDb.getCreditList(isCredit.value).then((list) => {
          creditList = list,
          for (var e in creditList) {total.value += e.amount!}
        });
    isDataProcessing.value = false;
    _groupItems();
    update();
  }

  void _groupItems() {
    groupedItems.clear();
    for (var item in creditList) {
      if (!groupedItems.containsKey(item.creditor)) {
        groupedItems[item.creditor!] = 0.0;
      }
      groupedItems[item.creditor!] =
          (groupedItems[item.creditor]! + item.amount!);
    }
    update();
  }
  // update

  // delete

  void updateIsCreditValue(value) {
    isCredit.value = value;
    getCreditListFromDb();
    update();
  }

  void onPageChanged(int index) {
    if (index == 0) {
      isCredit.value = true;
    } else {
      isCredit.value = false;
    }
    update();
    getCreditListFromDb();
  }

  void changePage(int index) {
    //isCredit.value = index == 0 ? true : false;
    //getCreditListFromDb();
    pageController.jumpToPage(index);
  }
}
