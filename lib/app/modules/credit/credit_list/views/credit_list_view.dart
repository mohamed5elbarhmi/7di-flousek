import 'dart:math';

import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:my_project/app/routes/app_pages.dart';
import 'package:my_project/app/shared/appBar.dart';
import 'package:my_project/app/shared/constant.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../controllers/credit_list_controller.dart';

class CreditListView extends GetView<CreditListController> {
  const CreditListView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CreditListController>(
        init: CreditListController(), //controller,
        builder: (c) => Directionality(
              textDirection: TextDirection.ltr,
              child: Scaffold(
                  appBar: getStandardAppBar("write_credit".tr),
                  floatingActionButton: FloatingActionButton.extended(
                    onPressed: () {
                      Get.toNamed(Routes.ADD_CREDIT,
                          arguments: [c.isCredit.value, '', null, true]);
                    },
                    label: Text(
                      "credit_add_button".tr,
                      style: TextStyle(fontSize: 18.sp),
                    ),
                    icon: Icon(
                      Icons.add,
                      size: 36.sp,
                    ),
                    backgroundColor: Color(0xfff009688),
                  ),
                  body: Center(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(color: inputBackColor),
                              borderRadius: BorderRadius.circular(0.r)),
                          //height: 46.h,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  //c.updateIsCreditValue(true);
                                  c.changePage(0);
                                },
                                child: Container(
                                  width: 198.w,
                                  height: 42.h,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                          bottom: BorderSide(
                                              width: 2.5.sp,
                                              color: c.isCredit.value
                                                  ? primaryColor
                                                  : Colors.white))),
                                  child: Center(
                                      child: Text(
                                    "credit_gave".tr,
                                    style: TextStyle(
                                        fontSize: 20.sp,
                                        color: c.isCredit.value
                                            ? primaryColor
                                            : Colors.grey,
                                        fontWeight: FontWeight.w500),
                                  )),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  //c.updateIsCreditValue(false);
                                  c.changePage(1);
                                },
                                child: Container(
                                  width: 198.w,
                                  height: 42.h,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border(
                                          bottom: BorderSide(
                                              width: 2.5.sp,
                                              color: !c.isCredit.value
                                                  ? primaryColor
                                                  : Colors.white))),
                                  child: Center(
                                      child: Text(
                                    "credit_took".tr,
                                    style: TextStyle(
                                        fontSize: 20.sp,
                                        color: !c.isCredit.value
                                            ? primaryColor
                                            : Colors.grey,
                                        fontWeight: FontWeight.w500),
                                  )),
                                ),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: PageView(
                            //pageSnapping: false,
                            controller: c.pageController,
                            children: [
                              getGaveCreditList(c),
                              getTookCreditList(c)
                            ],
                            onPageChanged: (index) {
                              log(index);
                              c.onPageChanged(index);
                            },
                          ),
                        ),
                      ]))),
            ));
  }
}

Widget getGaveCreditList(c) {
  return c.isDataProcessing.value
      ? LoadingAnimationWidget.newtonCradle(color: primaryColor, size: 240.w)
      : Column(
          children: [
            Container(
              width: 360.w,
              height: 50.h,
              color: Colors.white,
              child: Row(
                children: [
                  SizedBox(
                    width: 20.w,
                  ),
                  Container(
                    width: 40.w,
                    height: 40.w,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(100.r)),
                        border: Border.all(color: primaryColor)),
                    child: const Icon(
                      Icons.arrow_downward,
                      color: primaryColor,
                    ),
                  ),
                  SizedBox(
                    width: 60.w,
                  ),
                  Text(
                    "${"expenses_total".tr} : ${c.total.toString()} ${"dirham".tr}",
                    style: TextStyle(
                        fontSize: 20.sp,
                        fontWeight: FontWeight.w500,
                        color: primaryColor),
                  ),
                ],
              ),
            ),
            Container(
              color: const Color.fromARGB(255, 199, 199, 199),
              height: 6.h,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: c.groupedItems.length,
                itemBuilder: (BuildContext context, int index) {
                  final key = c.groupedItems.keys.toList()[index];
                  final value = c.groupedItems[key]!;
                  return GestureDetector(
                    onTap: () {
                      Get.toNamed(Routes.DETAILS_CREDIT,
                          arguments: [key, c.isCredit.value]);
                    },
                    child: Padding(
                      padding:
                          EdgeInsets.only(top: 20.h, left: 30.w, right: 30.w),
                      child: Container(
                        width: 320.w,
                        height: 40.h,
                        decoration: BoxDecoration(
                          color: inputBackColor,
                          borderRadius: BorderRadius.circular(8.r),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              "$key   :",
                              style: TextStyle(
                                  fontSize: 20.sp, fontWeight: FontWeight.w500),
                            ),
                            Text(
                              "$value  ${"dirham".tr}",
                              style: TextStyle(
                                  fontSize: 18.sp, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        );
}

Widget getTookCreditList(c) {
  return c.isDataProcessing.value
      ? LoadingAnimationWidget.newtonCradle(color: primaryColor, size: 240.w)
      : Column(
          children: [
            Container(
              width: 360.w,
              height: 50.h,
              color: Colors.white,
              child: Row(
                children: [
                  SizedBox(
                    width: 20.w,
                  ),
                  Container(
                    width: 40.w,
                    height: 40.w,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(100.r)),
                        border: Border.all(color: Colors.red)),
                    child: const Icon(
                      Icons.arrow_upward,
                      color: Colors.red,
                    ),
                  ),
                  SizedBox(
                    width: 60.w,
                  ),
                  Text(
                    "${"expenses_total".tr} : ${c.total.toString()} ${"dirham".tr}",
                    style: TextStyle(
                        fontSize: 20.sp,
                        fontWeight: FontWeight.w500,
                        color: Colors.red),
                  ),
                ],
              ),
            ),
            Container(
              color: const Color.fromARGB(255, 199, 199, 199),
              height: 6.h,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: c.groupedItems.length,
                itemBuilder: (BuildContext context, int index) {
                  final key = c.groupedItems.keys.toList()[index];
                  final value = c.groupedItems[key]!;
                  return GestureDetector(
                    onTap: () {
                      Get.toNamed(Routes.DETAILS_CREDIT,
                          arguments: [key, c.isCredit.value]);
                    },
                    child: Padding(
                      padding:
                          EdgeInsets.only(top: 20.h, left: 30.w, right: 30.w),
                      child: Container(
                        width: 320.w,
                        height: 40.h,
                        decoration: BoxDecoration(
                          color: inputBackColor,
                          borderRadius: BorderRadius.circular(8.r),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              "$key   :",
                              style: TextStyle(
                                  fontSize: 20.sp, fontWeight: FontWeight.w500),
                            ),
                            Text(
                              "$value Dh",
                              style: TextStyle(
                                  fontSize: 18.sp, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        );
}
