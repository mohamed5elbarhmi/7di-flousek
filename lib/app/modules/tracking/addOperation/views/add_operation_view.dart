import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:my_project/app/shared/appBar.dart';
import 'package:my_project/app/shared/constant.dart';
import 'package:my_project/app/shared/input_filed.dart';
import 'package:my_project/app/shared/validators.dart';

import '../controllers/add_operation_controller.dart';

class AddOperationView extends GetView<AddOperationController> {
  const AddOperationView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AddOperationController>(
        init: AddOperationController(), //controller,
        builder: (controller) => Scaffold(
            appBar: getStandardAppBar(controller.edit.value
                ? "tracking_edit_operation_page_title".tr
                : "tracking_add_operation_page_title".tr),
            body: SingleChildScrollView(
              child: Center(
                /* Directionality(
                  textDirection: Get.locale!.toLanguageTag() == "ar-AR"
                      ? TextDirection.rtl
                      : TextDirection.ltr,
                      */
                child: (Form(
                  key: controller.tracking_formKey,
                  child: Directionality(
                    textDirection: TextDirection.ltr,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 50.h,
                          ),
                          Container(
                            height: 46.h,
                            padding: EdgeInsets.only(left: 20.w),
                            width: 340.w,
                            decoration: BoxDecoration(
                                color: inputBackColor,
                                borderRadius: BorderRadius.circular(8.r),
                                border: Border.all(
                                    color: primaryColor, width: 2.sp)),
                            child: DropDownTextField(
                              controller: controller.categotyNameSingleDD,
                              textFieldDecoration: InputDecoration(
                                  contentPadding: EdgeInsets.only(
                                      top: 0.h, left: 2.w, right: 2.w),
                                  hintText: "tracking_chatr_category".tr),
                              dropDownList:
                                  controller.categoriesDropDownValueModel,
                              dropDownItemCount: 8,
                              // controller: controller.creditorNameController,
                              onChanged: (value) {
                                controller.categoryController.text =
                                    (value as DropDownValueModel).value;
                                controller.isOtherCategory(value);
                              },
                            ),
                          ),
                          SizedBox(
                            height: 30.h,
                          ),
                          controller.isOtherCat.value
                              ? Column(
                                  children: [
                                    getTextField(
                                      // ignore: prefer_const_constructors
                                      prefixIcon: Icon(
                                        Icons.category_rounded,
                                        color: primaryColor,
                                      ),
                                      controllerAttribut:
                                          controller.categoryController,
                                      hint: "tracking_chatr_category".tr,
                                      myValidator: ValidateInput.validator,
                                    ),
                                    SizedBox(
                                      height: 30.h,
                                    ),
                                  ],
                                )
                              : SizedBox(
                                  height: 0.h,
                                ),
                          getTextField(
                            keyboardType: TextInputType.datetime,
                            readOnly: true,
                            // ignore: prefer_const_constructors
                            prefixIcon: Icon(
                              Icons.date_range,
                              color: primaryColor,
                            ),
                            controllerAttribut: controller.dateController,
                            hint: "tracking_balance_tabale_date".tr,
                            onTap: () {
                              controller.chooseDate();
                            },
                            myValidator: ValidateInput.validator,
                          ),
                          SizedBox(
                            height: 30.h,
                          ),
                          getTextField(
                              keyboardType: TextInputType.phone,
                              // ignore: prefer_const_constructors
                              prefixIcon: Icon(
                                Icons.money_outlined,
                                color: primaryColor,
                              ),
                              controllerAttribut: controller.amountController,
                              hint: "credit_amount".tr,
                              myValidator: ValidateInput.validatorAmount),
                          SizedBox(
                            height: 30.h,
                          ),
                          getTextField(
                              maxline: 4,
                              // ignore: prefer_const_constructors
                              prefixIcon: Icon(
                                Icons.textsms,
                                color: primaryColor,
                              ),
                              controllerAttribut: controller.motifController,
                              hint: "credit_motif".tr,
                              myValidator: ValidateInput.validator),
                          SizedBox(
                            height: 40.h,
                          ),
                          Container(
                            width: 360.w,
                            height: 46.h,
                            decoration: BoxDecoration(
                                //color: secondaryColor,
                                borderRadius: BorderRadius.circular(10.r)),
                            child: ElevatedButton(
                              onPressed: controller.saveTrackingOpertaion,
                              style: ElevatedButton.styleFrom(
                                backgroundColor: primaryColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(6.r),
                                ),
                              ),
                              child: Text(
                                "tracking_save_operation".tr,
                                style: TextStyle(
                                    fontSize: 20.sp, color: Colors.white),
                              ),
                            ),
                          )
                        ]),
                  ),
                )),
              ),
            )));
  }
}
