import 'dart:developer';
import 'dart:isolate';
import 'dart:math';

import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:my_project/app/data/operationModel.dart';
import 'package:my_project/app/data/trackingCategoryModel.dart';
import 'package:my_project/app/database/categoryDbProvider.dart';
import 'package:my_project/app/database/operationDbProvider.dart';
import 'package:my_project/app/modules/tracking/trackingDetails/controllers/tracking_details_controller.dart';
import 'package:my_project/app/modules/tracking/trackingHome/controllers/tracking_home_controller.dart';
import 'package:my_project/app/routes/app_pages.dart';
import 'package:my_project/app/shared/constant.dart';

class AddOperationController extends GetxController {
  //TODO: Implement AddOperationController

  OperationDbProvider operationtDb = OperationDbProvider();
  CategoryDbProvider categoryDbProvider = CategoryDbProvider();
  final tracking_formKey = GlobalKey<FormState>();

  var catList = getTrackingCategory();
  RxList<TrackingCategoryModel> categories =
      RxList<TrackingCategoryModel>.empty(growable: true);
  RxList<DropDownValueModel> categoriesDropDownValueModel =
      RxList<DropDownValueModel>.empty(growable: true);
  var selectedDate = DateTime.now().obs;
  var edit = false.obs;

  // text controller
  var categotyNameSingleDD = SingleValueDropDownController();
  var categoryController = TextEditingController();
  var dateController = TextEditingController();
  var amountController = TextEditingController();
  var motifController = TextEditingController();

  var operationId = 0.obs;

  RxBool isOtherCat = false.obs;

  @override
  void onInit() {
    super.onInit();
    checkAndSaveCategories();
    initFormValues();
    update();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  List<DropDownValueModel> categoryDropDownList() {
    List<DropDownValueModel> list = [];
    categories.forEach((c) {
      String nameP = c.description == null
          ? c.name!.toString().tr
          : "${c.name!.toString().tr} : ${c.description.toString().tr}";
      list.add(DropDownValueModel(name: nameP, value: c.name));
    });
    return list;
  }

  chooseDate() async {
    DateTime? pickedDate = await showDatePicker(
      context: Get.context!,
      initialDate: selectedDate.value,
      firstDate: DateTime(2000),
      lastDate: DateTime(2024),
      helpText: 'Select DOB',
      cancelText: 'Close',
      confirmText: 'Confirm',
      errorFormatText: 'Enter valid date',
      errorInvalidText: 'Enter valid date range',
      fieldLabelText: 'DOB',
      fieldHintText: 'Month/Date/Year',
    );
    if (pickedDate != null && pickedDate != selectedDate.value) {
      selectedDate.value = pickedDate;
      dateController.text =
          DateFormat("dd/MM/y").format(selectedDate.value).toString();
      update();
    }
  }

  void saveTrackingOpertaion() async {
    if (tracking_formKey.currentState!.validate()) {
      if (isOtherCat.value) {
        var cat = TrackingCategoryModel(name: categoryController.text);
        await categoryDbProvider.addCategory(cat);
      }
      var operation = OperationModel(
          createdAt: selectedDate.value,
          motif: motifController.text,
          amount: double.parse(amountController.text),
          category: categoryController.text);

      //create credit
      if (!edit.value) {
        await operationtDb.addOperation(operation).then((value) => {
              if (value != null)
                {
                  Get.find<TrackingHomeController>()
                      .getOperationListFromDbByDate(),
                  Get.find<TrackingHomeController>().setCurrentMonthDate(),
                  Get.back()
                }
            });
      } else {
        operation.id = operationId.value;
        await operationtDb.updateOperationByID(operation).then((value) => {
              if (value != null)
                {
                  Get.find<TrackingHomeController>()
                      .getOperationListFromDbByDate(),
                  Get.find<TrackingDetailsController>()
                      .getOperationListByDateAndCategory(),
                  Get.find<TrackingDetailsController>()
                      .idElementDisplayed
                      .value = 0,
                  update(),
                  Get.back()
                }
            });
      }
    }
  }

  void initFormValues() async {
    edit.value = Get.arguments[0];
    if (edit.value) {
      OperationModel operation =
          await operationtDb.getOperationById(Get.arguments[2]);
      if (operation != null) {
        amountController.text = operation.amount.toString();
        motifController.text = operation.motif!;
        selectedDate.value = operation.createdAt!;
        dateController.text =
            DateFormat("dd/MM/y").format(selectedDate.value).toString();

        categoryController.text = operation.category!;
        categotyNameSingleDD.dropDownValue = DropDownValueModel(
            name: categoryController.text.toString().tr,
            value: categoryController.text);

        operationId.value = operation.id!;
      }
    } else {
      categoryController.text = Get.arguments[1];
      categotyNameSingleDD.dropDownValue = DropDownValueModel(
          name: categoryController.text.toString().tr,
          value: categoryController.text);
    }
    update();
  }

  void checkAndSaveCategories() async {
    await categoryDbProvider.getCategoryList().then((list) => {
          if (list.isNotEmpty)
            {
              categories.value = list,
              prepareDropDownCategoryList(list),
            }
          else
            {
              getTrackingCategory().forEach((c) async {
                await categoryDbProvider.addCategory(c);
              }),
              checkAndSaveCategories(),
            }
        });
  }

  void prepareDropDownCategoryList(List<TrackingCategoryModel> list) {
    categoriesDropDownValueModel.value = [];
    for (var c in list) {
      String nameP = c.description == null
          ? c.name!.toString().tr
          : "${c.name!.toString().tr} : ${c.description.toString().tr}";
      categoriesDropDownValueModel
          .add(DropDownValueModel(name: nameP, value: c.name));
    }
    categoriesDropDownValueModel
        .add(DropDownValueModel(name: "Other".tr, value: "Other"));
    update();
  }

  void deleteAllCategory() async {
    //await categoryDbProvider.deleteAllCategories();
  }

  void isOtherCategory(DropDownValueModel value) {
    if (value.value == "Other") {
      isOtherCat.value = true;
      categoryController.text = "";
    } else {
      isOtherCat.value = false;
    }
    update();
  }
}
