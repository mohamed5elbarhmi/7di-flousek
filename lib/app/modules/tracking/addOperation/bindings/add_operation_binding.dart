import 'package:get/get.dart';

import '../controllers/add_operation_controller.dart';

class AddOperationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddOperationController>(
      () => AddOperationController(),
    );
  }
}
