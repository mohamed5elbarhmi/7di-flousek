import 'dart:developer';
import 'dart:math';

import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:my_project/app/data/GDPData.dart';
import 'package:my_project/app/data/chartData.dart';
import 'package:my_project/app/data/monthModel.dart';
import 'package:my_project/app/data/operationModel.dart';
import 'package:my_project/app/database/operationDbProvider.dart';
import 'package:my_project/app/shared/constant.dart';

class TrackingHomeController extends GetxController {
  //TODO: Implement TrackingHomeController
  OperationDbProvider operationtDb = OperationDbProvider();
  List<OperationModel> opList = [];
  List<Map<String, dynamic>> opCategoryList = [];
  List<Map<String, Object?>> yearList = [];
  List<Map<String, Object?>> operationListGroupedByMount = [];

  var selectedYear = TextEditingController();
  var paletteColor = <Color>[].obs;
  var chartData = <GDPData>[].obs;
  var balanceChartData = <ChartData>[].obs;
  var total = 0.0.obs;
  var totalInYear = 1.0.obs;

  var page = 0.obs;
  var listIsEmpty = false.obs;
  PageController pageController = PageController();
  //var tabController = TabController(length: 2, vsync: vsync);

  var isDataProcessing = false.obs;
  var isCategoryChart = true.obs;
  var selectFromdDate = DateTime.now().obs;
  var selectToDate = DateTime.now().obs;
  var fromDateController = TextEditingController();
  var toDateController = TextEditingController();
  var currentYear = DateTime.now().year.obs;
  @override
  void onInit() {
    setCurrentMonthDate();
    getOperationListFromDbByDate();
    getBalanceData(currentYear.value.toString());
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void getOperationListFromDbByDate() async {
    isDataProcessing.value = true;
    if (selectFromdDate.value.isBefore(selectToDate.value)) {
      //opList = await operationtDb.getOperationListByDateInterval( selectFromdDate.value, selectToDate.value);
      //
      var test = await operationtDb.deleteOperationById(1024);
      await operationtDb
          .getCategoryAndSumByDateInterval(
              selectFromdDate.value, selectToDate.value)
          .then((arr) => {
                opCategoryList = arr,
                listIsEmpty.value = false,
                if (arr.isEmpty)
                  {
                    isDataProcessing.value = false,
                    listIsEmpty.value = true,
                    update()
                  }
              });

      getGdpData(opCategoryList);
      getPaletteColor();
    }

    isDataProcessing.value = false;
    update();
  }

  void setCurrentMonthDate() async {
    isDataProcessing.value = true;
    DateTime now = DateTime.now();
    selectFromdDate.value = DateTime(now.year, now.month, 1);
    fromDateController.text =
        DateFormat("dd-MM-y").format(selectFromdDate.value).toString();
    selectToDate.value = DateTime(now.year, now.month + 1, 0);
    toDateController.text =
        DateFormat("dd-MM-y").format(selectToDate.value).toString();
    yearList = await operationtDb.getYearList();

    isDataProcessing.value = false;
    update();
  }

  // date picker
  chooseFromDate() async {
    DateTime? pickedDate = await showDatePicker(
      context: Get.context!,
      initialDate: selectFromdDate.value,
      firstDate: DateTime(2000),
      lastDate: DateTime(2024),
      helpText: 'Select DOB',
      cancelText: 'Close',
      confirmText: 'Confirm',
      errorFormatText: 'Enter valid date',
      errorInvalidText: 'Enter valid date range',
      fieldLabelText: 'DOB',
      fieldHintText: 'Month/Date/Year',
    );
    if (pickedDate != null && pickedDate != selectFromdDate.value) {
      selectFromdDate.value = pickedDate;
      fromDateController.text =
          DateFormat("dd-MM-y").format(selectFromdDate.value).toString();
      //
      getOperationListFromDbByDate();
      update();
    }
  }

  chooseToDate() async {
    DateTime? pickedDate = await showDatePicker(
      context: Get.context!,
      initialDate: selectToDate.value,
      firstDate: DateTime(2000),
      lastDate: DateTime(2024),
      helpText: 'Select DOB',
      cancelText: 'Close',
      confirmText: 'Confirm',
      errorFormatText: 'Enter valid date',
      errorInvalidText: 'Enter valid date range',
      fieldLabelText: 'DOB',
      fieldHintText: 'Month/Date/Year',
    );
    if (pickedDate != null && pickedDate != selectToDate.value) {
      selectToDate.value = pickedDate;
      toDateController.text =
          DateFormat("dd-MM-y").format(selectToDate.value).toString();

      getOperationListFromDbByDate();
      update();
    }
  }

  void getGdpData(List<Map<String, dynamic>> list) {
    chartData.value = [];
    total.value = 0;
    list.forEach((element) {
      chartData.add(GDPData(
          content: element["category"].toString().tr,
          gdp: double.parse(element["totalAmount"].toStringAsFixed(2))));
      total.value += double.parse(element["totalAmount"].toStringAsFixed(2));
    });
    update();
  }

  void getPaletteColor() {
    paletteColor.value = [];
    paletteColor.add(Color.fromRGBO(24, 118, 242, 1));
    paletteColor.add(Color.fromRGBO(0, 167, 74, 1));
    paletteColor.add(Color.fromRGBO(252, 193, 59, 1));
    paletteColor.add(Color.fromRGBO(236, 88, 115, 1));
    paletteColor.add(Color.fromRGBO(124, 161, 242, 1));
    paletteColor.add(Color.fromRGBO(0, 168, 181, 1));
    paletteColor.add(Color.fromRGBO(133, 109, 88, 1));
    paletteColor.add(Color.fromRGBO(255, 134, 0, 1));

    if (opCategoryList.length > 8) {
      final random = Random();

      for (var i = 0; i < (opCategoryList.length - 8); i++) {
        paletteColor.add(Color.fromARGB(
          255,
          random.nextInt(256),
          random.nextInt(256),
          random.nextInt(256),
        ));
      }
    }
    update();
  }

  List<DropDownValueModel> yearDropDownList() {
    List<DropDownValueModel> list = [];

    yearList.forEach((elm) {
      String yearObject = elm['year'] as String;
      list.add(DropDownValueModel(name: yearObject, value: yearObject));
    });
    update();

    return list;
  }

  void getBalanceData(String value) async {
    isDataProcessing.value = true;
    currentYear.value = int.parse(value);
    operationListGroupedByMount = await operationtDb
        .getOperationsByYearAndMonth(int.parse(currentYear.value.toString()));
    operationListGroupedByMount.isNotEmpty ? totalInYear.value = 0.0 : null;
    operationListGroupedByMount.forEach((element) {
      totalInYear.value += double.parse(element["totalAmount"].toString());
    });
    getChartData(operationListGroupedByMount);

    isDataProcessing.value = false;
    update();
  }

  String getMonthNAmeByMnumber(String n) {
    List<monthModel> list = getMonthList();
    //return list.fil

    return list.firstWhere((m) => m.id == int.parse(n)).name!;
  }

  void getChartData(List<Map<String, dynamic>> list) {
    balanceChartData.value = [];

    list.forEach((e) {
      balanceChartData.add(ChartData(
          x: int.parse(e["month"].toString()),
          y: double.parse(e["totalAmount"].toStringAsFixed(2))));
    });
    update();
  }

  void changePage(int index) {
    if (!isClosed) {
      isCategoryChart.value = index == 0 ? true : false;
      pageController.jumpToPage(index);
      update();
    }
  }

  void generateData() async {
    var listt = getTrackingList();
    listt.forEach((e) async {
      OperationModel elm = OperationModel.fromJson(e);
      await operationtDb.addOperation(elm);
    });
  }
}
