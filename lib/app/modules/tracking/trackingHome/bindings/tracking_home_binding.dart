import 'package:get/get.dart';

import '../controllers/tracking_home_controller.dart';

class TrackingHomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TrackingHomeController>(
      () => TrackingHomeController(),
    );
  }
}
