import 'dart:developer';

import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:my_project/app/data/GDPData.dart';
import 'package:my_project/app/data/chartData.dart';
import 'package:my_project/app/routes/app_pages.dart';
import 'package:my_project/app/shared/appBar.dart';
import 'package:my_project/app/shared/constant.dart';
import 'package:my_project/app/shared/input_filed.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../controllers/tracking_home_controller.dart';

class TrackingHomeView extends GetView<TrackingHomeController> {
  const TrackingHomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<TrackingHomeController>(
        init: TrackingHomeController(), //controller,
        builder: (c) => Directionality(
              textDirection: TextDirection.ltr,
              child: Scaffold(
                  appBar: getStandardAppBar("track_money".tr),
                  floatingActionButton: Directionality(
                    textDirection: Get.locale!.toLanguageTag() == "ar-AR"
                        ? TextDirection.rtl
                        : TextDirection.ltr,
                    child: FloatingActionButton.extended(
                      onPressed: () {
                        Get.toNamed(Routes.ADD_OPERATION,
                            arguments: [false, "", null]);
                      },
                      label: Text(
                        "expense_add_button".tr,
                        style: TextStyle(fontSize: 18.sp),
                      ),
                      icon: Icon(Icons.add, size: 36.sp),
                      backgroundColor: primaryColor,
                    ),
                  ),
                  body: SingleChildScrollView(
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  c.changePage(0);
                                },
                                child: Container(
                                  width: 200.w,
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: c.isCategoryChart.value
                                                  ? primaryColor
                                                  : const Color.fromARGB(
                                                      255, 205, 205, 205),
                                              width: 2.sp))),
                                  child: Center(
                                    child: Padding(
                                      padding: EdgeInsets.all(10.r),
                                      child: Text(
                                        "tracking_chatr_category".tr,
                                        style: TextStyle(
                                            color: c.isCategoryChart.value
                                                ? primaryColor
                                                : Colors.grey,
                                            fontSize: 20.sp,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  c.changePage(1);
                                },
                                child: Container(
                                  width: 200.w,
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: !c.isCategoryChart.value
                                                  ? primaryColor
                                                  : const Color.fromARGB(
                                                      255, 205, 205, 205),
                                              width: 2.sp))),
                                  child: Center(
                                    child: Padding(
                                      padding: EdgeInsets.all(10.r),
                                      child: Text(
                                        "tracking_chatrt_balance".tr,
                                        style: TextStyle(
                                            color: !c.isCategoryChart.value
                                                ? primaryColor
                                                : Colors.grey,
                                            fontSize: 20.sp,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 4.h,
                          ),
                          Container(
                            height: 680.h,
                            child: PageView(
                              pageSnapping: false,
                              controller: c.pageController,
                              children: [getPieChartPage(c), getBalancePage(c)],
                              onPageChanged: (index) {
                                if (index == 0) {
                                  c.isCategoryChart.value = true;
                                } else {
                                  c.isCategoryChart.value = false;
                                }

                                c.update();
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  )),
            ));
  }
}

Widget getPieChartPage(c) {
  return ListView(children: [
    Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            height: 80.w,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  height: 40.h,
                  width: 160.w,
                  decoration: BoxDecoration(
                      border: Border.all(color: primaryColor, width: 2.sp),
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.r)),
                  child: getTextField(
                    fillColor: Colors.white,
                    borderRadius: 10.r,
                    contentPadding: EdgeInsets.only(left: 6.w, top: 4.h),
                    //contentPadding = EdgeInsets.only(left: 6, top: 14),
                    keyboardType: TextInputType.datetime,
                    readOnly: true,
                    // ignore: prefer_const_constructors
                    prefixIcon: Icon(
                      Icons.date_range,
                      color: primaryColor,
                    ),
                    controllerAttribut: c.fromDateController,
                    hint: TRACKING_FROM_DATE,
                    onTap: () {
                      c.chooseFromDate();
                    },
                    myValidator: null,
                  ),
                ),
                Container(
                  height: 40.h,
                  width: 160.w,
                  decoration: BoxDecoration(
                      border: Border.all(color: primaryColor, width: 2.sp),
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.r)),
                  child: getTextField(
                    fillColor: Colors.white,
                    borderRadius: 10.r,
                    contentPadding: EdgeInsets.only(left: 6.w, top: 4.h),
                    keyboardType: TextInputType.datetime,
                    readOnly: true,
                    // ignore: prefer_const_constructors
                    prefixIcon: Icon(
                      Icons.date_range,
                      color: primaryColor,
                    ),
                    controllerAttribut: c.toDateController,
                    hint: TRACKING_TO_DATE,
                    onTap: () {
                      c.chooseToDate();
                    },
                    myValidator: null,
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: 10.h,
        ),
        Column(
          children: [
            Container(
              width: 380.w,
              //height: 360.h,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.r),
                border:
                    Border.all(color: const Color.fromARGB(255, 210, 210, 210)),
                boxShadow: [
                  BoxShadow(
                    color: const Color.fromARGB(255, 210, 210, 210),
                    blurRadius: 4,
                    offset: Offset(1, 2), // Shadow position
                  ),
                ],
              ),
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 10.h),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          height: 10.h,
                        ),
                        !c.isDataProcessing.value
                            ? (c.paletteColor.length != 0
                                ? SizedBox(
                                    height: 400.h,
                                    child: SfCircularChart(
                                      palette: c.paletteColor,
                                      title: ChartTitle(
                                          text:
                                              "${"expenses_total".tr}: ${c.total.value.toStringAsFixed(2)} ${"dirham".tr} ",
                                          textStyle: TextStyle(
                                            fontWeight: FontWeight.w600,
                                          )),
                                      legend: Legend(
                                          position: LegendPosition.bottom,
                                          isVisible: true,
                                          overflowMode:
                                              LegendItemOverflowMode.wrap),
                                      tooltipBehavior:
                                          TooltipBehavior(enable: true),
                                      series: <CircularSeries>[
                                        DoughnutSeries<GDPData, String>(
                                          dataSource: c.chartData,
                                          xValueMapper: (GDPData data, _) =>
                                              data.content,
                                          yValueMapper: (GDPData data, _) =>
                                              data.gdp,
                                          dataLabelSettings: DataLabelSettings(
                                              isVisible: true),
                                          enableTooltip: true,
                                        )
                                      ],
                                    ),
                                  )
                                : Text(""))
                            : LoadingAnimationWidget.newtonCradle(
                                color: primaryColor, size: 240.w),
                      ],
                    ),
                  ),
                  c.listIsEmpty.value
                      ? SizedBox(
                          height: 20.h,
                          child: Text("tracking_chart_no_data_available".tr))
                      : Text(""),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10.h),
              child: Container(
                width: 380.w,
                //height: 360.h,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(top: BorderSide(color: Colors.grey))),

                child: Column(
                  children: [
                    SizedBox(
                      height: 10.h,
                    ),
                    Wrap(
                      spacing: 10.sp,
                      children: [
                        for (var i = 0; i < c.opCategoryList.length; i++)
                          GestureDetector(
                            onTap: () {
                              Get.toNamed(Routes.TRACKING_DETAILS, arguments: [
                                c.opCategoryList[i]["category"].toString(),
                                double.parse(c.opCategoryList[i]["totalAmount"]
                                    .toStringAsFixed(2)),
                                c.paletteColor[i],
                                c.selectFromdDate.value.toString(),
                                c.selectToDate.value.toString()
                              ]);
                            },
                            child: Padding(
                              padding: EdgeInsets.only(top: 2.h, bottom: 2.h),
                              child: Container(
                                width: 180.w,
                                //height: 80.h,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10.r),
                                  border: Border.all(color: Color(0xfffd2d2d2)),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xfffd2d2d2),
                                      blurRadius: 4,
                                      offset: Offset(1, 2), // Shadow position
                                    ),
                                  ],
                                ),
                                child: Directionality(
                                  textDirection:
                                      Get.locale!.toLanguageTag() == "ar-AR"
                                          ? TextDirection.rtl
                                          : TextDirection.ltr,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            left: 10.w, right: 10.w, top: 10.h),
                                        child: Text(
                                          c.opCategoryList[i]["category"]
                                              .toString()
                                              .tr,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18.sp,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10.h,
                                      ),
                                      Directionality(
                                        textDirection: TextDirection.ltr,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            Text(
                                              "${double.parse(c.opCategoryList[i]["totalAmount"].toStringAsFixed(2))} ${"dirham".tr}",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18.sp,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                            Text(
                                              (c.opCategoryList[i]
                                                              ["totalAmount"] *
                                                          100 /
                                                          c.total.value)
                                                      .toStringAsFixed(2) +
                                                  " %",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18.sp,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 6.h,
                                      ),
                                      Container(
                                        height: 10.h,
                                        width: 180.w,
                                        decoration: BoxDecoration(
                                            color: c.paletteColor[i],
                                            borderRadius: BorderRadius.only(
                                                bottomLeft:
                                                    Radius.circular(10.r),
                                                bottomRight:
                                                    Radius.circular(10.r))),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )
                      ],
                    ),
                    SizedBox(
                      height: 100.h,
                    )
                  ],
                ),
              ),
            ),
          ],
        )
      ],
    ),
  ]);
}

Widget getBalancePage(c) {
  return ListView(children: [
    Column(
      children: [
        SizedBox(
          height: 20.h,
        ),
        Container(
          height: 40.h,
          padding: EdgeInsets.only(left: 20.w),
          width: 240.w,
          decoration: BoxDecoration(
              border: Border.all(color: primaryColor, width: 2.sp),
              color: Colors.white,
              borderRadius: BorderRadius.circular(14.r)),
          child: DropDownTextField(
            textStyle: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 18.sp),
            textFieldDecoration: InputDecoration(
                hintStyle: TextStyle(color: Colors.black),
                border: InputBorder.none,
                hintText: "tracking_balance_year".tr,
                contentPadding: EdgeInsets.only(top: 0.h)),
            dropDownList: c.yearDropDownList(),
            dropDownItemCount: 8,
            //controller: c.currentYear,
            onChanged: (value) {
              c.getBalanceData(value.value);
            },
          ),
        ),
        SizedBox(
          height: 20.h,
        ),
        Container(
            padding: EdgeInsets.only(top: 10.h, bottom: 10.h, right: 10.w),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.r),
              border:
                  Border.all(color: const Color.fromARGB(255, 210, 210, 210)),
              boxShadow: [
                BoxShadow(
                  color: const Color.fromARGB(255, 210, 210, 210),
                  blurRadius: 4,
                  offset: Offset(1, 2), // Shadow position
                ),
              ],
            ),
            child: SfCartesianChart(
              primaryXAxis: NumericAxis(interval: 1, minimum: 0, maximum: 12),
              primaryYAxis: NumericAxis(
                interval: 1000,
                minimum: 0,
              ),
              series: <ChartSeries<ChartData, int>>[
                ColumnSeries<ChartData, int>(
                    dataSource: c.balanceChartData,
                    xValueMapper: (ChartData data, _) => data.x,
                    yValueMapper: (ChartData data, _) => data.y),
              ],
            )),
        SizedBox(
          height: 10.h,
        ),
        Container(
          width: 360.w,
          child: Column(
            children: [
              SizedBox(
                height: 10.h,
              ),
              Container(
                padding: EdgeInsets.only(top: 10.h),
                width: 360.w,
                child: Directionality(
                  textDirection: Get.locale!.toLanguageTag() == "ar-AR"
                      ? TextDirection.rtl
                      : TextDirection.ltr,
                  child: Table(
                    children: [
                      TableRow(
                          decoration: BoxDecoration(
                              border: Border(bottom: BorderSide(width: 1.sp))),
                          children: [
                            Padding(
                              padding: EdgeInsets.only(bottom: 8.h, top: 8.h),
                              child: Center(
                                child: Text(
                                  "tracking_balance_tabale_date".tr,
                                  style: TextStyle(
                                      fontSize: 20.sp, color: textgrayColor),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 8.h, top: 8.h),
                              child: Center(
                                child: Text(
                                  "${"tracking_balance_tabale_expense".tr} (${"dirham".tr})",
                                  style: TextStyle(
                                      fontSize: 20.sp, color: textgrayColor),
                                ),
                              ),
                            ),
                          ]),
                      for (var i = 0;
                          i < c.operationListGroupedByMount.length;
                          i++)
                        TableRow(
                            decoration: BoxDecoration(
                                border:
                                    Border(bottom: BorderSide(width: 1.sp))),
                            children: [
                              Padding(
                                padding: EdgeInsets.only(bottom: 4.h, top: 4.h),
                                child: Center(
                                  child: Column(
                                    children: [
                                      Text(c
                                          .getMonthNAmeByMnumber(c
                                              .operationListGroupedByMount[i]
                                                  ["month"]
                                              .toString())
                                          .toString()
                                          .tr),
                                      SizedBox(
                                        height: 4.h,
                                      ),
                                      Text(
                                        c.operationListGroupedByMount[i]
                                                    ["month"]
                                                .toString() +
                                            " - " +
                                            c.operationListGroupedByMount[i]
                                                    ["year"]
                                                .toString(),
                                        style: TextStyle(
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.w400,
                                            color: const Color.fromARGB(
                                                255, 119, 119, 119)),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding:
                                    EdgeInsets.only(bottom: 14.h, top: 14.h),
                                child: Center(
                                  child: Text(
                                    "${c.operationListGroupedByMount[i]["totalAmount"].toStringAsFixed(1)}",
                                    style: TextStyle(
                                        fontSize: 18.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.black),
                                  ),
                                ),
                              ),
                            ]),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 110.h,
        )
      ],
    ),
  ]);
}
