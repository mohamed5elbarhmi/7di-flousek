import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:my_project/app/data/operationModel.dart';
import 'package:my_project/app/database/operationDbProvider.dart';
import 'package:my_project/app/shared/constant.dart';

class TrackingDetailsController extends GetxController {
  //TODO: Implement TrackingDetailsController
  OperationDbProvider operationtDb = OperationDbProvider();

  late var category;
  late var amount;
  late Color borderColor;
  late var fromDate;
  late var toDate;

  var idElementDisplayed = 0.obs;

  List<OperationModel> opListByCategory = [];
  @override
  void onInit() {
    initVariables();
    getOperationListByDateAndCategory();
  }

  void initVariables() {
    category = Get.arguments[0];
    amount = Get.arguments[1].toString();
    borderColor = Get.arguments[2];
    fromDate = Get.arguments[3];
    toDate = Get.arguments[4];
    update();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void getOperationListByDateAndCategory() async {
    var totalAmount = 0.0;
    await operationtDb
        .getOperationListByDateIntervalAndCategory(
            DateTime.parse(fromDate), DateTime.parse(toDate), category)
        .then((arr) => {
              opListByCategory = arr,
              arr.forEach((e) {
                totalAmount += e.amount!;
              }),
            });
    amount = totalAmount.toStringAsFixed(1);
    update();
  }

  String prepareCreatedAt(DateTime value) {
    return DateFormat("dd/MM/y").format(value).toString();
  }

  void showDetail(int value) {
    idElementDisplayed.value = idElementDisplayed.value != value ? value : 0;
    update();
  }

  Future showDeleteModal(opId) {
    return Get.defaultDialog(
      title: "",
      content: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.warning,
            color: Colors.red,
            size: 40,
          ),
          SizedBox(
            height: 14.h,
          ),
          Text(
            "credit_delete_message".tr,
            style: TextStyle(fontSize: 16),
          ),
          SizedBox(
            height: 16.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                child: Text("credit_delete_cancel".tr),
                style: ElevatedButton.styleFrom(
                  primary: Colors.grey,
                  elevation: 0,
                ),
                onPressed: () {
                  Get.back();
                },
              ),
              ElevatedButton(
                child: Text("credit_delete_confirm".tr),
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  elevation: 0,
                ),
                onPressed: () {
                  deleteOperation(opId);
                  Get.back();
                },
              ),
            ],
          )
        ],
      ),
    );
  }

  void deleteOperation(id) async {
    await operationtDb.deleteOperationById(id);
    getOperationListByDateAndCategory();
  }
}
