import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:my_project/app/data/operationModel.dart';
import 'package:my_project/app/routes/app_pages.dart';
import 'package:my_project/app/shared/appBar.dart';
import 'package:my_project/app/shared/constant.dart';

import '../controllers/tracking_details_controller.dart';

class TrackingDetailsView extends GetView<TrackingDetailsController> {
  const TrackingDetailsView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<TrackingDetailsController>(
      init: TrackingDetailsController(), //controller,
      builder: (c) => Directionality(
        textDirection: TextDirection.ltr,
        child: Scaffold(
          appBar: getStandardAppBar("tracking_detail_operation_page_title".tr),
          floatingActionButton: FloatingActionButton.extended(
            onPressed: () {
              Get.toNamed(Routes.ADD_OPERATION,
                  arguments: [false, c.category, null]);
            },
            label: Text(
              "expense_add_button".tr,
              style: TextStyle(fontSize: 18.sp),
            ),
            icon: Icon(Icons.add, size: 36.sp),
            backgroundColor: primaryColor,
          ),
          body: Center(
              child: Column(
            children: [
              SizedBox(
                height: 20.h,
              ),
              Container(
                width: 360.w,
                height: 60.h,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.r),
                  border: Border.all(color: Color(0xfffd2d2d2)),
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xfffd2d2d2),
                      blurRadius: 4,
                      offset: Offset(1, 2), // Shadow position
                    ),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      height: 4.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            SizedBox(
                              width: 4.w,
                            ),
                            Icon(
                              Icons.pie_chart,
                              color: c.borderColor,
                            ),
                            SizedBox(
                              width: 8.w,
                            ),
                            Padding(
                              padding: EdgeInsets.only(left: 10.w),
                              child: Text(
                                c.category.toString().tr,
                                style: TextStyle(
                                    fontSize: 20.sp,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(right: 10.w, left: 10.w),
                          child: Text(
                            "${c.amount} ${"dirham".tr}",
                            style: TextStyle(
                                fontSize: 20.sp, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      height: 6.h,
                      width: 357.w,
                      decoration: BoxDecoration(
                          color: c.borderColor,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10.r),
                              bottomRight: Radius.circular(10.r))),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Scrollbar(
                  child: ListView.builder(
                    itemCount: c.opListByCategory.length,
                    itemBuilder: (BuildContext context, int index) {
                      final OperationModel item = c.opListByCategory[index];
                      return Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              c.showDetail(c.opListByCategory[index].id!);
                            },
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: 20.h, left: 30.w, right: 30.w),
                              child: Container(
                                width: 320.w,
                                height: 40.h,
                                decoration: BoxDecoration(
                                  color: inputBackColor,
                                  borderRadius: BorderRadius.circular(8.r),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(
                                      "${c.prepareCreatedAt(item.createdAt!)}   :",
                                      style: TextStyle(
                                          fontSize: 18.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Text(
                                      "${item.amount}  ${"dirham".tr}",
                                      style: TextStyle(
                                          fontSize: 18.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    c.idElementDisplayed.value == item.id
                                        ? Icon(
                                            Icons.keyboard_arrow_up,
                                            size: 40.w,
                                          )
                                        : Icon(
                                            Icons.keyboard_arrow_down,
                                            size: 40.w,
                                          )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          if (c.idElementDisplayed.value == item.id)
                            Container(
                              width: 320.w,
                              height: 90.h,
                              decoration: BoxDecoration(
                                color: detailBlockColor,
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10.r),
                                    bottomRight: Radius.circular(10.r)),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 5.h,
                                  ),
                                  SizedBox(
                                    height: 35.h,
                                    child: Text(
                                      "${"credit_motif".tr} : ${item.motif!}",
                                      style: TextStyle(
                                          fontSize: 18.sp,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 50.h,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        IconButton(
                                          icon: Icon(
                                            Icons.edit_document,
                                            color: Colors.blue,
                                            size: 30.w,
                                          ),
                                          onPressed: () {
                                            Get.toNamed(Routes.ADD_OPERATION,
                                                arguments: [
                                                  true,
                                                  "",
                                                  c.opListByCategory[index].id
                                                ]);
                                          },
                                        ),
                                        IconButton(
                                          icon: Icon(
                                            Icons.delete,
                                            color: Colors.red,
                                            size: 30.w,
                                          ),
                                          onPressed: () {
                                            //c.deleteCredit(item.id);
                                            c.showDeleteModal(item.id);
                                          },
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                        ],
                      );
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 70.h,
              )
            ],
          )),
        ),
      ),
    );
  }
}
