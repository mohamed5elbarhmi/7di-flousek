import 'package:get/get.dart';

import '../controllers/tracking_details_controller.dart';

class TrackingDetailsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TrackingDetailsController>(
      () => TrackingDetailsController(),
    );
  }
}
