import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/tracking_list_controller.dart';

class TrackingListView extends GetView<TrackingListController> {
  const TrackingListView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('TrackingListView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'TrackingListView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
