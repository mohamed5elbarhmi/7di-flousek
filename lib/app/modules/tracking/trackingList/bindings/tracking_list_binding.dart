import 'package:get/get.dart';

import '../controllers/tracking_list_controller.dart';

class TrackingListBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TrackingListController>(
      () => TrackingListController(),
    );
  }
}
