part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const CREDIT_LIST = _Paths.CREDIT_LIST;
  static const ADD_CREDIT = _Paths.ADD_CREDIT;
  static const DETAILS_CREDIT = _Paths.DETAILS_CREDIT;
  static const TRACKING_HOME = _Paths.TRACKING_HOME;
  static const TRACKING_LIST = _Paths.TRACKING_LIST;
  static const TRACKING_DETAILS = _Paths.TRACKING_DETAILS;
  static const ADD_OPERATION = _Paths.ADD_OPERATION;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const CREDIT_LIST = '/credit-list';
  static const ADD_CREDIT = '/add-credit';
  static const DETAILS_CREDIT = '/details-credit';
  static const TRACKING_HOME = '/tracking-home';
  static const TRACKING_LIST = '/tracking-list';
  static const TRACKING_DETAILS = '/tracking-details';
  static const ADD_OPERATION = '/add-operation';
}
