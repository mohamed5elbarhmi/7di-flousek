import 'package:get/get.dart';

import '../modules/tracking/addOperation/bindings/add_operation_binding.dart';
import '../modules/tracking/addOperation/views/add_operation_view.dart';
import '../modules/credit/add_credit/bindings/add_credit_binding.dart';
import '../modules/credit/add_credit/views/add_credit_view.dart';
import '../modules/credit/credit_list/bindings/credit_list_binding.dart';
import '../modules/credit/credit_list/views/credit_list_view.dart';
import '../modules/credit/details_credit/bindings/details_credit_binding.dart';
import '../modules/credit/details_credit/views/details_credit_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/tracking/trackingDetails/bindings/tracking_details_binding.dart';
import '../modules/tracking/trackingDetails/views/tracking_details_view.dart';
import '../modules/tracking/trackingHome/bindings/tracking_home_binding.dart';
import '../modules/tracking/trackingHome/views/tracking_home_view.dart';
import '../modules/tracking/trackingList/bindings/tracking_list_binding.dart';
import '../modules/tracking/trackingList/views/tracking_list_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.CREDIT_LIST,
      page: () => const CreditListView(),
      binding: CreditListBinding(),
    ),
    GetPage(
      name: _Paths.ADD_CREDIT,
      page: () => AddCreditView(),
      binding: AddCreditBinding(),
    ),
    GetPage(
      name: _Paths.DETAILS_CREDIT,
      page: () => const DetailsCreditView(),
      binding: DetailsCreditBinding(),
    ),
    GetPage(
      name: _Paths.TRACKING_HOME,
      page: () => const TrackingHomeView(),
      binding: TrackingHomeBinding(),
    ),
    GetPage(
      name: _Paths.TRACKING_LIST,
      page: () => const TrackingListView(),
      binding: TrackingListBinding(),
    ),
    GetPage(
      name: _Paths.TRACKING_DETAILS,
      page: () => const TrackingDetailsView(),
      binding: TrackingDetailsBinding(),
    ),
    GetPage(
      name: _Paths.ADD_OPERATION,
      page: () => const AddOperationView(),
      binding: AddOperationBinding(),
    ),
  ];
}
