import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_project/app/data/language.dart';
import 'package:my_project/app/data/monthModel.dart';
import 'package:my_project/app/data/trackingCategoryModel.dart';

// credit Titles

const String HOME_TITLE = "Welcome To ";
const String APP_TITLE = "Ra9eb Flousek";
const String TRACK_MONEY = "Expense monitoring";
const String WRITE_CREDIT = "Credit monitoring";
const String CREDIT_GAVE = "To Collect";
const String CREDIT_TOOK = "To Give";
const String CREDITOR_NAME = "Moul Credit";
const String CREDIT_AMOUNT = "Amount";
const String CREDIT_MOTIF = "Motif";
const String CREDIT_DATE = "Date";
const String CREDITOR_NAME_SELECT = "Creditor";
const String CREDIT_DELETE_MESSAGE = "Are you sure you want to delete it !";
const String CREDIT_DELETE_CONFIRM = "Confirm";
const String CREDIT_DELETE_CANCEL = "Cancel";
const String CREDIT_ADD_BUTTON = "Add credit";

// tracking  titles

const String TRACKING_CHATR_CATEGORY = "Category";
const String TRACKING_CHATRT_BALANCE = "Balance";
const String TRACKING_SAVE_OPERATION = "Save";
const String TRACKING_OTHER_CATEGORY = "Other";

const String TRACKING_CATEGORY_MONTH = "Month";
const String TRACKING_CATEGORY_YEAR = "Year";
const String TRACKING_FROM_DATE = "From Date";
const String TRACKING_TO_DATE = "To Date";

const String TRACKING_BALANCE_YEAR = "Select year";
const String TRACKING_BALANCE_TABALE_DATE = "Date";
const String TRACKING_BALANCE_TABALE_EXPENSE = "Expense";
const String TRACKING_CHART_NO_DATA_AVAILABLE = "You have no operation saved !";
const String EXPENSE_ADD_BUTTON = "Add expense";

// page titles

const String CREDIT_LIST_PAGE_TITLE = "Credit book";
const String ADD_CREDIT_PAGE_TITLE = "Add credit";

const String EDIT_CREDIT_PAGE_TITLE = "Edit credit";
const String DETAIL_CREDIT_PAGE_TITLE = "Credit details";
const String TRACKING_HOME_PAGE_TITLE = "Chart";
const String TRACKING_ADD_OPERATION_PAGE_TITLE = "Add Operation";
const String TRACKING_DETAIL_OPERATION_PAGE_TITLE = "Expense details";

// Colors
//const Color primaryColor = Color(0xfff42a5f5);
const Color primaryColor = Color(0xfff009688);
const Color secondaryColor = Color(0xffff59342);

//const Color chartTitleColor = Color.fromARGB(255, 130, 161, 235);
const Color chartTitleColor = Color(0xffff59342);
//const Color primaryColor = Color(0xfffe87676);
//const Color secondaryColor = Color(0xfffF4C361);
const Color inputBackColor = Color(0xfffEFEEEE);
const Color errorColor = Color(0xffff0000);
const Color detailBlockColor = Color.fromARGB(255, 231, 231, 231);
const Color textgrayColor = Color.fromARGB(255, 121, 121, 121);

// imagesPath

const homeLogoPath = "assets/images/logo_app_2.png";
const trackMoneyImgPath = "assets/images/track_money3.png";
const creditImgPath = "assets/images/credit_book.png";

// category list

List<TrackingCategoryModel> getTrackingCategory() {
  var list = <TrackingCategoryModel>[];
  list.add(
      TrackingCategoryModel(id: 1, name: "Housing", description: "Rent_Bills"));
  list.add(
      TrackingCategoryModel(id: 2, name: "Food", description: "Dining_Coffee"));
  list.add(TrackingCategoryModel(id: 3, name: "Transportation"));
  list.add(TrackingCategoryModel(
      id: 4, name: "Healthcare", description: "Medical_Expenses"));
  list.add(TrackingCategoryModel(
      id: 5, name: "Debts", description: "Loan_Repayments"));
  list.add(TrackingCategoryModel(id: 6, name: "Shopping"));
  list.add(TrackingCategoryModel(
      id: 7, name: "Education", description: "Books_Courses"));
  list.add(TrackingCategoryModel(
      id: 8, name: "Entertainment", description: "Hobbies_Cinema"));
  list.add(TrackingCategoryModel(id: 9, name: "Dating"));
  list.add(TrackingCategoryModel(id: 10, name: "Business"));
  list.add(TrackingCategoryModel(id: 11, name: "Savings_Investments"));
  list.add(TrackingCategoryModel(id: 12, name: "Unexpected_Expenses"));

  /*
  list.add(TrackingCategoryModel(id: 2, name: "Bills"));
  list.add(TrackingCategoryModel(id: 3, name: "Food"));
  list.add(TrackingCategoryModel(id: 4, name: "Shopping"));
  list.add(TrackingCategoryModel(id: 5, name: "Clothing"));
  list.add(TrackingCategoryModel(id: 6, name: "Sport"));
  list.add(TrackingCategoryModel(id: 7, name: "Education"));
  //list.add(TrackingCategoryModel(id: 7, name: "gift"));
  list.add(TrackingCategoryModel(id: 8, name: "Other"));
  */
  return list;
}

List<Map<String, dynamic>> getTrackingList() {
  List<String> categories = [
    "Home",
    "Bills",
    "Food",
    "Shopping",
    "Clothing",
    "Sport",
    "Education",
    "Other"
  ];
  List<Map<String, dynamic>> sampleItems = [];

  Random random = Random();

  for (int i = 1; i <= 40; i++) {
    String randomCategory = categories[random.nextInt(categories.length)];
    int randomDay = random.nextInt(31) + 1; // Random day between 1 and 31
    String createdAt =
        "2023-08-${randomDay.toString().padLeft(2, '0')} ${random.nextInt(24).toString().padLeft(2, '0')}:${random.nextInt(60).toString().padLeft(2, '0')}:${random.nextInt(60).toString().padLeft(2, '0')}.${random.nextInt(1000).toString().padLeft(3, '0')}";
    double amount = (random.nextDouble() * 200)
        .roundToDouble(); // Random amount between 0 and 200

    Map<String, dynamic> item = {
      "id": i,
      "category": randomCategory,
      "createdAt": createdAt,
      "motif": "test",
      "amount": amount,
    };

    sampleItems.add(item);
  }

  return sampleItems;
}

List<monthModel> getMonthList() {
  List<monthModel> months = [];
  List<Map<String, String>> monthsMap = [
    {"id": "01", "name": "Jan"},
    {"id": "02", "name": "Feb"},
    {"id": "03", "name": "Mar"},
    {"id": "04", "name": "Apr"},
    {"id": "05", "name": "May"},
    {"id": "06", "name": "Jun"},
    {"id": "07", "name": "Jul"},
    {"id": "08", "name": "Aug"},
    {"id": "09", "name": "Sep"},
    {"id": "10", "name": "Oct"},
    {"id": "11", "name": "Nov"},
    {"id": "12", "name": "Dec"},
  ];

  for (var element in monthsMap) {
    months.add(monthModel.fromJson(element));
  }

  return months;
}

List<Language> getLanguageList() {
  return [
    Language(name: "العربية", locale: const Locale('ar', 'AR')),
    Language(name: "English", locale: const Locale('en', 'EN'))
  ];
}
