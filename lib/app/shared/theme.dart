import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:my_project/app/shared/constant.dart';

ThemeData lightTheme = ThemeData.light().copyWith(
    brightness: Brightness.light,
    primaryColorLight: const Color(0xffefeeee),
    primaryColor: const Color(0xfff009688),
    textTheme: TextTheme(
        headlineSmall: TextStyle(
            color: primaryColor, fontSize: 23.sp, fontWeight: FontWeight.w500),
        titleLarge: TextStyle(color: Colors.black, fontSize: 23.sp)));
/*
ThemeData lightTheme = ThemeData.light().copyWith(
    brightness: Brightness.light,
    primaryColorLight: const Color(0xffefeeee),
    primaryColor: const Color(0xfff009688),
    textTheme: TextTheme(
        headlineSmall: TextStyle(color: primaryColor, fontSize: 23.sp),
        titleLarge: TextStyle(color: Colors.black, fontSize: 23.sp)));

ThemeData darkTheme = ThemeData.dark().copyWith(
    brightness: Brightness.dark,
    primaryColorLight: Color(0xfffa6a6a6),
    primaryColor: const Color(0xfff009688),
    textTheme: TextTheme(
        headlineSmall: TextStyle(color: primaryColor, fontSize: 23.sp),
        titleLarge: TextStyle(color: Colors.grey, fontSize: 23.sp)));

*/
