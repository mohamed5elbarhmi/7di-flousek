import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'constant.dart';

Widget getTextField(
    {required String hint,
    required String? Function(String?)? myValidator,
    required TextEditingController? controllerAttribut,
    int maxline = 1,
    onChangedFunction,
    bool? obscure,
    Icon? prefixIcon,
    bool? readOnly = false,
    focusNode,
    onTap,
    Color fillColor = inputBackColor,
    double borderRadius = 8.0,
    EdgeInsetsGeometry contentPadding = const EdgeInsets.only(left: 6, top: 14),
    TextInputType? keyboardType = TextInputType.text,
    IconButton? suffixicon}) {
  return SizedBox(
    width: 360.w,

    //height: 46.h,
    child: TextFormField(
        keyboardType: keyboardType,
        readOnly: readOnly!,
        maxLines: maxline,
        focusNode: focusNode,
        onChanged: onChangedFunction,
        style: TextStyle(fontSize: 18.sp),
        decoration: InputDecoration(
            focusedBorder: UnderlineInputBorder(
                //<-- SEE HERE
                borderSide: BorderSide(width: 0.sp, color: primaryColor),
                borderRadius: BorderRadius.circular(borderRadius)),
            enabledBorder: UnderlineInputBorder(
              //<-- SEE HERE
              borderRadius: BorderRadius.circular(borderRadius),
              borderSide: BorderSide(width: 0.sp, color: Colors.white),
            ),
            contentPadding: contentPadding,
            filled: true,
            fillColor: fillColor,
            hintText: hint,
            hintStyle: TextStyle(
              fontSize: 18.sp,
              fontWeight: FontWeight.w400,
            ),
            prefixIcon: prefixIcon,
            suffixIcon: suffixicon),
        validator: myValidator,
        controller: controllerAttribut,
        onTap: onTap,
        obscureText: obscure ?? false),
  );
}
