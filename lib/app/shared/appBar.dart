import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:my_project/app/modules/tracking/addOperation/controllers/add_operation_controller.dart';
import 'package:my_project/app/modules/tracking/trackingHome/controllers/tracking_home_controller.dart';
import 'package:my_project/app/shared/constant.dart';

PreferredSizeWidget getStandardAppBar(title) {
  return PreferredSize(
    preferredSize: Size.fromHeight(50.h),
    child: Directionality(
      textDirection: TextDirection.ltr,
      child: AppBar(
        elevation: 1.0,
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 22.sp,
                  fontWeight: FontWeight.w500),
            ),
            GestureDetector(
              child: Container(
                width: 34.w,
                height: 34.w,
                decoration: BoxDecoration(
                    color: primaryColor,
                    borderRadius: BorderRadius.all(Radius.circular(10.r)),
                    border: Border.all(width: 2.sp, color: primaryColor)),
                child: Icon(
                  Icons.language,
                  size: 26.sp,
                  color: Colors.white,
                ),
              ),
              onTap: () {
                Get.defaultDialog(
                  title: "",
                  content: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.language,
                        color: primaryColor,
                        size: 30.sp,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "change_language".tr,
                        style: TextStyle(
                            fontSize: 18.sp, fontWeight: FontWeight.w400),
                      ),
                      SizedBox(
                        height: 20.h,
                      ),
                      Directionality(
                        textDirection: TextDirection.rtl,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            for (var ln in getLanguageList())
                              GestureDetector(
                                onTap: () {
                                  Get.updateLocale(ln.locale!);

                                  if (Get.isRegistered<
                                      TrackingHomeController>()) {
                                    Get.find<TrackingHomeController>()
                                        .getGdpData(
                                            Get.find<TrackingHomeController>()
                                                .opCategoryList);
                                  }
                                  if (Get.isRegistered<
                                      AddOperationController>()) {
                                    Get.find<AddOperationController>()
                                        .prepareDropDownCategoryList(
                                            Get.find<AddOperationController>()
                                                .categories);
                                  }
                                  Get.back();
                                },
                                child: Container(
                                  width: 100.w,
                                  height: 40.h,
                                  decoration: BoxDecoration(
                                      color: Get.locale!.toLanguageTag() ==
                                              ln.locale!.toLanguageTag()
                                          ? primaryColor
                                          : inputBackColor,
                                      borderRadius:
                                          BorderRadius.circular(10.r)),
                                  child: Center(
                                      child: Text(
                                    ln.name!,
                                    style: TextStyle(
                                      fontSize: 18.sp,
                                      color: Get.locale!.toLanguageTag() ==
                                              ln.locale!.toLanguageTag()
                                          ? Colors.white
                                          : Colors.black,
                                    ),
                                  )),
                                ),
                              )
                          ],
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
          ],
        ),
        centerTitle: true,
      ),
    ),
  );
}
