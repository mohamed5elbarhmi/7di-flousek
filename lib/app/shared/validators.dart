import 'package:get/get.dart';

class ValidateInput {
  static String? validator(String? value) {
    if (value!.isEmpty) {
      return 'This filed is required';
    }
    return null;
  }

  static String? validatorAmount(String? value) {
    double result = 0.0;
    if (value!.isEmpty) {
      return 'This filed is required';
    }
    try {
      result = double.parse(value);
      //if (result.isEqual(0.0) || result <= 0) {
      //return "Please enter a numeric value";
    } catch (e) {
      return "Please enter a numeric value";
    }
    return null;
  }
}
